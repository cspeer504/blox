﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class MVCCreator : EditorWindow
{
    public string m_strObjectName = "Object";

    // Modify these if you wish to change any defaults. 
    private string m_strBaseNameSpace = "Blox";                                    //TODO: Rename this to whatever name space you want.
    private string m_strDestinationPathName = "BoxBlox/Scripts/MVCs";                      //TODO: Put the folder you want MVCs created in here. This is relative to the "Assets" folder
    private string m_strTemplatePathName = "Assets/CodeControl/Templates";
    private string m_strModelTemplateFileName = "ModelTemplate.txt";
    private string m_strViewTemplateFileName = "ViewTemplate.txt";
    private string m_strControllerTemplateFileName = "ControllerTemplate.txt";

    private string m_strLastObjectName = "";
    //Create a menu item in the "Windows" tool menu.
    [MenuItem("Window/Template Generation/Code Control MVC Creator")]           //Feel free to change where this menu option lives.
    public static void CreateNewMVC()
    {
        EditorWindow.GetWindow(typeof(MVCCreator));
    }


    void OnGUI()
    {
        // Show the Pop Up Window
        GUILayout.Label("This will create Code Control MVC Files.");
        GUILayout.Space(10);

        GUILayout.BeginHorizontal();
        GUILayout.Label("Object Name", new GUILayoutOption[0]);
        m_strObjectName = EditorGUILayout.TextField(m_strObjectName, new GUILayoutOption[0]);
        GUILayout.EndHorizontal();
        GUILayout.Space(10);

        string strObjectFilePrefix = m_strObjectName.Replace(" ", "");
        string strModelFile      = "Assets/" + m_strDestinationPathName     + "/" + m_strObjectName + "/" + strObjectFilePrefix + "Model"      + ".cs";
        string strViewFile       = "Assets/" + m_strDestinationPathName     + "/" + m_strObjectName + "/" + strObjectFilePrefix + "View"       + ".cs";
        string strControllerFile = "Assets/" + m_strDestinationPathName     + "/" + m_strObjectName + "/" + strObjectFilePrefix + "Controller" + ".cs";

        GUILayout.BeginHorizontal();
        GUILayout.Label("The following files will be generated:");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label(strModelFile);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label(strViewFile);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label(strControllerFile);
        GUILayout.EndHorizontal();

        GUILayout.Space(3);

        if(m_strObjectName.Length == 0)
        {
            m_strLastObjectName = m_strObjectName;
            GUI.color = Color.red;
            GUILayout.Label("Object name can not be empty");
        }
        else if(m_strLastObjectName != m_strObjectName && //So we don't run IsValidFolder check every iteration.
                AssetDatabase.IsValidFolder("Assets/" + m_strDestinationPathName + "/" + m_strObjectName))
        {
            GUI.color = Color.yellow;
            GUILayout.Label("MVC directory already exists for " + m_strObjectName + ".");
            GUILayout.Label("Delete directory or chose a new object name.");
        }
        else if(AssetDatabase.IsValidFolder("Assets/" + m_strDestinationPathName) == false)
        {
            GUI.color = Color.red;
            GUILayout.Label("The Destination Folder does not exist! Create this folder:");
            GUILayout.Label("Assets/" + m_strDestinationPathName);
        }
        else
        {
            m_strLastObjectName = m_strObjectName;
            GUI.color = Color.green;
            if (GUILayout.Button("Generate Files!!", new GUILayoutOption[0]) && m_strObjectName.Length > 0)
            {
                CreateANewMVC();
                m_strLastObjectName = "";
            }
        }
    }
    
    private void CreateANewMVC()
    {   
        if (!AssetDatabase.IsValidFolder(m_strTemplatePathName))
        {
            Debug.LogError("Template folder does not exist, please check the MVCCreatorEditor and modify m_strTemplatePathName to point to the folder of the MVC templates: \n" +
                Application.dataPath + "/" + m_strTemplatePathName);
            return;
        }

        // Ya, This is a double check. Just in case... Accidental clobbering sucks.
        if (AssetDatabase.IsValidFolder("Assets/" + m_strDestinationPathName + "/" + m_strObjectName))
        {
            Debug.LogWarning("Did not create MVC assets because target directory already exists. This prevents accidental clobbering. Path: " +
                            "Assets/" + m_strDestinationPathName + "/" + m_strObjectName);
            return;
        }

        // It's possible the folder is created and all templates aren't found, but we will live with that. 
        AssetDatabase.CreateFolder("Assets/" + m_strDestinationPathName, m_strObjectName);

        string strObjectFilePrefix = m_strObjectName.Replace(" ", "");
        string strModelFile      = m_strDestinationPathName + "/" + m_strObjectName + "/" + strObjectFilePrefix + "Model"       + ".cs";
        string strViewFile       = m_strDestinationPathName + "/" + m_strObjectName + "/" + strObjectFilePrefix + "View"        + ".cs";
        string strControllerFile = m_strDestinationPathName + "/" + m_strObjectName + "/" + strObjectFilePrefix + "Controller"  + ".cs";
            
        CreateFileFromTemplate(m_strModelTemplateFileName, strModelFile);
        CreateFileFromTemplate(m_strViewTemplateFileName, strViewFile);
        CreateFileFromTemplate(m_strControllerTemplateFileName, strControllerFile);

        //Refresh the Asset Database
        AssetDatabase.Refresh();
    }

    private void CreateFileFromTemplate(string strTemplateFileName, string strOutputFileName)
    {
        TextAsset templateTextFile = AssetDatabase.LoadAssetAtPath(m_strTemplatePathName + "/" + strTemplateFileName, typeof(TextAsset)) as TextAsset;
        string contents = "";

        if (templateTextFile != null)
        {
            contents = templateTextFile.text;
            contents = contents.Replace("OBJECT_NAME_HERE", m_strObjectName.Replace(" ", ""));
            contents = contents.Replace("NAMESPACE_NAME_HERE", m_strBaseNameSpace);
            
            // Create a new file. 
            using (StreamWriter sw = new StreamWriter(Application.dataPath + "/" + strOutputFileName))
            {
                sw.Write(contents);
            }
        }
        else
        {
            Debug.LogError("Can't find " + m_strTemplatePathName + "/" + strTemplateFileName + ". Need a template to create a file from.");
        }
    }
}