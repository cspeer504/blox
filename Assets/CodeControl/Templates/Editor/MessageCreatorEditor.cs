﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class MessageCreator : EditorWindow
{
    public string m_strMessageObjectName = "Event";

    // Modify these if you wish to change any defaults. 
    private string m_strBaseNameSpace = "Blox";                                    //TODO: Rename this to whatever name space you want.
    private string m_strDestinationPathName = "BoxBlox/Scripts/Messages";                      //TODO: Put the folder you want Message scripts created in here. This is relative to the "Assets" folder
    private string m_strTemplatePathName = "Assets/CodeControl/Templates";
    private string m_strModelTemplateFileName = "MessageTemplate.txt";

    private string m_strLastMessageName = "";
    //Create a menu item in the "Windows" tool menu.
    [MenuItem("Window/Template Generation/Code Control Message Creator")]           //Feel free to change where this menu option lives.
    public static void CreateNewMessage()
    {
        EditorWindow.GetWindow(typeof(MessageCreator));
    }


    void OnGUI()
    {
        // Show the Pop Up Window
        GUILayout.Label("This will create Code Control Message Files.");
        GUILayout.Space(10);

        GUILayout.BeginHorizontal();
        GUILayout.Label("Message Name", new GUILayoutOption[0]);
        m_strMessageObjectName = EditorGUILayout.TextField(m_strMessageObjectName, new GUILayoutOption[0]);
        GUILayout.EndHorizontal();
        GUILayout.Space(10);

        string strObjectFilePrefix = m_strMessageObjectName.Replace(" ", "");
        string strMessageFile = "Assets/" + m_strDestinationPathName + "/" + strObjectFilePrefix + "Message" + ".cs";

        GUILayout.BeginHorizontal();
        GUILayout.Label("The following file will be generated:");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label(strMessageFile);
        GUILayout.EndHorizontal();

        GUILayout.Space(3);

        if (m_strMessageObjectName.Length == 0)
        {
            m_strLastMessageName = m_strMessageObjectName;
            GUI.color = Color.red;
            GUILayout.Label("Object name can not be empty");
        }
        else if (m_strLastMessageName != m_strMessageObjectName && //So we don't run IsValidFolder check every iteration.
            AssetDatabase.LoadAssetAtPath(strMessageFile, typeof(TextAsset)) != null)
        {
            GUI.color = Color.yellow;
            GUILayout.Label("File already exists: " + m_strMessageObjectName + "Message.cs");
            GUILayout.Label("Delete file or chose a new message name.");
        }
        else if (AssetDatabase.IsValidFolder("Assets/" + m_strDestinationPathName) == false)
        {
            GUI.color = Color.red;
            GUILayout.Label("The Destination Folder does not exist! Create this folder:");
            GUILayout.Label("Assets/" + m_strDestinationPathName);
        }
        else
        {
            m_strLastMessageName = m_strMessageObjectName;
            GUI.color = Color.green;
            if (GUILayout.Button("Generate Files!!", new GUILayoutOption[0]) && m_strMessageObjectName.Length > 0)
            {
                CreateFilesFromTemplate();
                m_strLastMessageName = "";
            }
        }
    }

    private void CreateFilesFromTemplate()
    {
        if (!AssetDatabase.IsValidFolder(m_strTemplatePathName))
        {
            Debug.LogError("Template folder does not exist, please check the MessageCreator and modify m_strTemplatePathName to point to the folder of the templates: \n" +
                Application.dataPath + "/" + m_strTemplatePathName);
            return;
        }
        
        string strObjectFilePrefix = m_strMessageObjectName.Replace(" ", "");
        string strMessageFile = m_strDestinationPathName + "/" + strObjectFilePrefix + "Message" + ".cs";

        CreateFileFromTemplate(m_strModelTemplateFileName, strMessageFile);

        //Refresh the Asset Database
        AssetDatabase.Refresh();
    }

    private void CreateFileFromTemplate(string strTemplateFileName, string strOutputFileName)
    {
        TextAsset templateTextFile = AssetDatabase.LoadAssetAtPath(m_strTemplatePathName + "/" + strTemplateFileName, typeof(TextAsset)) as TextAsset;
        string contents = "";

        if (templateTextFile != null)
        {
            contents = templateTextFile.text;
            contents = contents.Replace("MESSAGE_NAME_HERE", m_strMessageObjectName.Replace(" ", ""));
            contents = contents.Replace("NAMESPACE_NAME_HERE", m_strBaseNameSpace);

            // Create a new file. 
            using (StreamWriter sw = new StreamWriter(Application.dataPath + "/" + strOutputFileName))
            {
                sw.Write(contents);
            }
        }
        else
        {
            Debug.LogError("Can't find " + m_strTemplatePathName + "/" + strTemplateFileName + ". Need a template to create a file from.");
        }
    }
}