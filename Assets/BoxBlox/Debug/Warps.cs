﻿using System.ComponentModel;
using UnityEngine;

public partial class SROptions
{
    [Category("Warps")] // Options will be grouped by category
    public void MainMenu()
    {
        Debug.Log("Enabling Warp: Goto MainMenu");
        Blox.Messages.LoadLevelMessage.Send(new Blox.Messages.LoadLevelMessage(Blox.Types.LevelID.MainMenu));
    }
}