﻿using System.ComponentModel;
using UnityEngine;

public partial class SROptions
{
    [Category("Cheats")] // Options will be grouped by category
    public void Randomizer()
    {
        Debug.Log("Enabling Cheat: Randomizer");
        Blox.Messages.BoardActionMessage.Send(new Blox.Messages.BoardActionMessage(Blox.Types.BoardAction.Randomize));
    }

    [Category("Cheats")]
    public void Colorblind()
    {
        Debug.Log("Enabling Cheat: Colorblind");
        Blox.Messages.BoardActionMessage.Send(new Blox.Messages.BoardActionMessage(Blox.Types.BoardAction.Colorblind));
    }

    [Category("Cheats")]
    public void TimeBoost()
    {
        Debug.Log("Enabling Cheat: TimeBoost");
        Blox.Messages.AddRoundTimeMessage.Send(new Blox.Messages.AddRoundTimeMessage(0, 0.25f));
    }

    [Category("Cheats")]
    public void ColorBoost()
    {
        Debug.Log("Enabling Cheat: ColorBoost");
        Blox.Messages.BoardActionMessage.Send(new Blox.Messages.BoardActionMessage(Blox.Types.BoardAction.ColorBoost));
    }

    [Category("Cheats")]
    public void QuantityBoost()
    {
        Debug.Log("Enabling Cheat: QuantityBoost");
        Blox.Messages.BoardActionMessage.Send(new Blox.Messages.BoardActionMessage(Blox.Types.BoardAction.QuantityBoost));
    }

    [Category("Cheats")]
    public void ClearBoosts()
    {
        Debug.Log("Enabling Cheat: ClearBoosts");
        Blox.Messages.BoardActionMessage.Send(new Blox.Messages.BoardActionMessage(Blox.Types.BoardAction.Clear));
    }
    //Messages.AddRoundTimeMessage
}