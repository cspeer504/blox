﻿using UnityEngine;
using System.Collections;

namespace Blox.Types
{
    public enum BigWinType
    {
        Nice = 0,
        Sweet,
        Awesome,
        Bonus,
        Insane
    }
}