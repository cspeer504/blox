﻿using UnityEngine;
using System.Collections;

namespace Blox.Types
{
    public enum MatchColor
    {
        Red,
        Blue,
        Green,
        Yellow
    }
}