﻿using UnityEngine;
using System.Collections;

namespace Blox.Types
{
    public struct PowerUp
    {
        public enum Type
        {
            None = -1,
            Randomizer,
            Colorblind,
            TimeBoost,
            ColorBoost,
            QuantityBoost
        };

        public Type eID;
        public string strName;
        public string strDescription;
        public string strTips;
    };
}