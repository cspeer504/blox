﻿using UnityEngine;
using System.Collections;

namespace Blox.Types
{
    public enum CountdownType
    {
        CountdownOne = 0,
        CountdownTwo,
        CountdownThree,
        CountdownGo
    }
}