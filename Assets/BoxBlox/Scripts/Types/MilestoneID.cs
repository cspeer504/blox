﻿using UnityEngine;
using System.Collections;

namespace Blox.Types
{
    public enum MilestoneID
    {
        Intro = 0,
        MainMenu,
        MultiGameSettings,
        MultiPowerSettings,
        MultiGameStart,
        MultiGameEnd,
        MultiGameScore
    }
}