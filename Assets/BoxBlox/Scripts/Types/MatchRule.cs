﻿using UnityEngine;
using System.Collections;

namespace Blox.Types
{
    public enum MatchRule
    {
        SameNodeType,
        SurroundingNodes
    }
}