﻿using UnityEngine;
using System.Collections;

namespace Blox.Types
{
    public enum MatchType
    {
        Normal,
        ColorBoostRed25,
        ColorBoostBlue25,
        ColorBoostGreen25,
        ColorBoostYellow25,
        QuantityBoost1

    }
}