﻿using UnityEngine;
using System.Collections;

namespace Blox.Types
{
    public enum LevelID
    {
        Initialize,
        Intro,
        MainMenu,
        Multiplayer,
        PowerUpMenu
    }
}