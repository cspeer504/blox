﻿using UnityEngine;
using System.Collections;

namespace Blox.Types
{
    public enum WinnerCallout
    {
        WinnerIs = 0,
        WinnerCongrats,
        WinnerBluePlayer,
        WinnerRedPlayer
    }
}