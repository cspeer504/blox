﻿using UnityEngine;
using System.Collections;

namespace Blox.Types
{
    public enum PlayerID
    {
        None = -1,
        One = 0,
        Two
    }
}