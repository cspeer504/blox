﻿using UnityEngine;
using System.Collections;

namespace Blox.Types
{
    public enum BoardAction
    {
        None = -1,
        Randomize,
        Colorblind,
        TimeBoost,
        ColorBoost,
        QuantityBoost,
        Clear
    }
}