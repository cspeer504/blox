﻿using UnityEngine;
using System.Collections;

namespace Blox.Types
{
    public enum TileColor
    {
        Blue,
        Green,
        Yellow,
        Red
    }
}