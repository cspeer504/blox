﻿using UnityEngine;
using System.Collections;

namespace Blox
{
    public class LevelService : MonoBehaviour
    {

        public Texture2D emptyProgressBar; // Set this in inspector.
        public Texture2D fullProgressBar; // Set this in inspector.
        public Types.LevelID initialLevel = Types.LevelID.Intro;

        private AsyncOperation async = null; // When assigned, load is in progress.
        private Types.LevelID m_strLevelLoading;

        public void Start()
        {
            DontDestroyOnLoad(this);
            StartCoroutine("LoadStartingLevel");
        }

        public void OnLoadLevel(Messages.LoadLevelMessage levelInfo)
        {
            Messages.LoadLevelMessage.Unregister(OnLoadLevel);
            m_strLevelLoading = levelInfo.levelID;
            StartCoroutine("LoadALevel");
        }

        private IEnumerator LoadStartingLevel()
        {
            yield return new WaitForSeconds(0.05f);
            OnLoadLevel(new Messages.LoadLevelMessage(initialLevel));
        }

        private IEnumerator LoadALevel()
        {
            yield return null;
            async = Application.LoadLevelAsync(m_strLevelLoading.ToString());
            yield return async;
            async = null;
            Messages.LoadLevelMessage.Register(OnLoadLevel);
            Messages.LevelLoadedMessage.Send(new Messages.LevelLoadedMessage(m_strLevelLoading));
        }

        void OnGUI()
        {
            /*
            if (async != null)
            {
                float fLength = 500;
                float fMiddleOfScreen = (Screen.width / 2 - 50) - fLength / 2;
                GUI.skin.label.alignment = TextAnchor.MiddleCenter;
                GUI.DrawTexture(new Rect(fMiddleOfScreen, Screen.height - 12, fLength, 8), emptyProgressBar);
                GUI.DrawTexture(new Rect(fMiddleOfScreen, Screen.height - 10, fLength * async.progress, 4), fullProgressBar);
                GUI.Label(new Rect(fMiddleOfScreen, Screen.height - 30, fLength, 20), string.Format("Loading: {0:N0}%", async.progress * 100f));
            }
            */
        }
    }
}
