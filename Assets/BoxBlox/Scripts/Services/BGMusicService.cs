﻿using UnityEngine;
using System.Collections;

namespace Blox
{
    public class BGMusicService : MonoBehaviour
    {

        public AudioSource introAudio;
        public AudioSource multiplayerAudio;

        private AudioSource nowPlaying;

        public void Start()
        {
            DontDestroyOnLoad(this);
            Messages.MilestoneMessage.Register(OnMilestone);
        }

        public void OnMilestone(Messages.MilestoneMessage msg)
        {
            switch(msg.Milestone)
            {
                case Types.MilestoneID.Intro:
                    PlayMusic(introAudio);
                    break;
                case Types.MilestoneID.MainMenu:
                    PlayMusic(introAudio);
                    break;
                case Types.MilestoneID.MultiGameSettings:
                    break;
                case Types.MilestoneID.MultiPowerSettings:
                    break;
                case Types.MilestoneID.MultiGameStart:
                    PlayMusic(multiplayerAudio);
                    break;
                case Types.MilestoneID.MultiGameEnd:
                    break;
                case Types.MilestoneID.MultiGameScore:
                    break;
                default:
                    break;
            }
        }
        private void PlayMusic(AudioSource aClip)
        {
            if(nowPlaying != aClip)
            {
                StopAllAudio();
                aClip.Play();
                nowPlaying = aClip;
            }
        }

        private void StopAllAudio()
        {
            introAudio.Stop();
            multiplayerAudio.Stop();
        }
        
    }
}
