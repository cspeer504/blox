﻿using UnityEngine;
using System.Collections;


namespace Blox.Services
{
    public class ServiceLoader : MonoBehaviour
    {
        public GameObject[] servicesToLoad;
        //** I'm not sure what this guy will do yet? Maybe load services?       

        void Start()
        {
            DontDestroyOnLoad(this);
            foreach (GameObject go in servicesToLoad)
            {
                GameObject objService = Instantiate(go);
                objService.transform.SetParent(gameObject.transform);
                DontDestroyOnLoad(objService);
            }
        }
    }
}