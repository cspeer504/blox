﻿using UnityEngine;
using System.Collections;

namespace Blox.Services
{
    public class PlayerService : MonoBehaviour
    {
        Controllers.PlayerManagementController m_objPlayerManager = null;
        
        // Use this for initialization
        void Start()
        {
            DontDestroyOnLoad(this);
            Models.PlayerManagementModel objPlayerManagerModel = new Models.PlayerManagementModel();
            m_objPlayerManager = CodeControl.Controller.Instantiate<Controllers.PlayerManagementController>(Resources.Load("Prefabs/Managers/PlayerManager"), objPlayerManagerModel);
            m_objPlayerManager.transform.SetParent(gameObject.transform);
        }

        public Controllers.PlayerController GetPlayerOne()
        {
            return m_objPlayerManager.GetPlayer(0);
        }

        public Controllers.PlayerController GetPlayerTwo()
        {
            return m_objPlayerManager.GetPlayer(1);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
