﻿using UnityEngine;
using System.Collections;

namespace Blox
{
    public class IntroSceneManager : MonoBehaviour
    {
        public Types.LevelID m_eNextScene = Types.LevelID.MainMenu;
        //public GameObject[] m_aSplashScreens;
        public SpriteRenderer[] m_aSplashScreens;
        public float[] m_aSplashTimes;
        //private int m_iCounter = 0;


        /* Animation progress (transparency in our case) */
        private float progress;

        // Use this for initialization
        void Start()
        {
            Messages.MilestoneMessage.Send(new Messages.MilestoneMessage(Types.MilestoneID.Intro));
            //m_iCounter = 0;
            StartCoroutine("ShowSplash");
            //StartSplash();
        }

        // Update is called once per frame
        void Update()
        {

        }
        /*
        private void StartSplash()
        {
            float fTime = (m_iCounter < m_aSplashTimes.Length) ? m_aSplashTimes[m_iCounter] : 3f;

            if (m_iCounter < m_aSplashScreens.Length)
            {
                iTween.FadeTo(m_aSplashScreens[m_iCounter], iTween.Hash("alpha", 1.0f, "time", fTime, "oncompletetarget", gameObject, "oncomplete", "OnFadeInComplete"));
            }
            else
            {
                Messages.LoadLevelMessage.Send(new Messages.LoadLevelMessage(m_eNextScene));                
            }
        }
        private void onFadeInComplete()
        {
            float fTime = (m_iCounter < m_aSplashTimes.Length) ? m_aSplashTimes[m_iCounter] : 3f;
            iTween.FadeTo(m_aSplashScreens[m_iCounter], iTween.Hash("alpha", 0.0f, "time", fTime, "oncompletetarget", gameObject, "oncomplete", "OnFadeOutComplete"));
        }

        public void OnFadeOutComplete()
        {
            m_iCounter++;
            StartSplash();
        }*/
        
        private IEnumerator ShowSplash()
        {
            yield return new WaitForSeconds(0.9f);

            for (int i = 0; i < m_aSplashScreens.Length; i++)
            {

                //Make all the fancy stuff happen
                var solidColor = new Color(m_aSplashScreens[i].color.r, m_aSplashScreens[i].color.g, m_aSplashScreens[i].color.b, 1);
                var transparentColor = new Color(solidColor.r, solidColor.g, solidColor.b, 0);
                m_aSplashScreens[i].color = transparentColor;

                while (progress < 1f)
                {
                    m_aSplashScreens[i].color = Color.Lerp(transparentColor, solidColor, progress);
                    progress += 0.5f * Time.deltaTime;
                    yield return null;
                }
                
                if (i < m_aSplashTimes.Length)
                {
                    yield return new WaitForSeconds(m_aSplashTimes[i]);
                }
                else
                {
                    yield return new WaitForSeconds(3f);
                }
                
                while (progress > 0f)
                {
                    m_aSplashScreens[i].color = Color.Lerp(transparentColor, solidColor, progress);
                    progress -= 0.5f * Time.deltaTime;
                    yield return null;
                }
            }
            Messages.LoadLevelMessage.Send(new Messages.LoadLevelMessage(m_eNextScene)); //** TODO, why are there two load level messages in this class?

        }
    }
}