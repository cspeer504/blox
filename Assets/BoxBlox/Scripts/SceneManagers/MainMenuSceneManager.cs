﻿using UnityEngine;
using System.Collections;

namespace Blox
{
    public class MainMenuSceneManager : MonoBehaviour {
        Models.MainMenuBoardModel m_objMainMenuBoardModel;
        public SpriteRenderer screenCover;
        private float progress;

        // Use this for initialization
        void Start()
        {
            m_objMainMenuBoardModel = new Models.MainMenuBoardModel();
            CodeControl.Controller.Instantiate<Controllers.MainMenuBoardController>(Resources.Load("Prefabs/Boards/TitleBoard"), m_objMainMenuBoardModel); //** TODO: Change to correct resource folder.
            StartCoroutine("RevealScene");
            Messages.MilestoneMessage.Send(new Messages.MilestoneMessage(Types.MilestoneID.MainMenu));
        }

        // Update is called once per frame
        void Update() {

        }

        public void OnButtonPressSinglePlayer()
        {
            //Messages.LoadLevelMessage.Send(new Messages.LoadLevelMessage("SinglePlayer"));
            Debug.LogError("Single player not yet implemented!");
        }
        public void OnButtonPressMultiPlayer()
        {
            m_objMainMenuBoardModel.Delete();
            Messages.LoadLevelMessage.Send(new Messages.LoadLevelMessage(Types.LevelID.Multiplayer));
            //Messages.LoadLevelMessage.Send(new Messages.LoadLevelMessage(Types.LevelID.PowerUpMenu));
        }

        public void OnButtonPressExit()
        {
            Application.Quit();
        }


        private IEnumerator RevealScene()
        {
            var solidColor = new Color(screenCover.color.r, screenCover.color.g, screenCover.color.b, 1);
            var transparentColor = new Color(solidColor.r, solidColor.g, solidColor.b, 0);
            screenCover.color = solidColor;
            progress = 1f;

            while (progress > 0f)
            {
                screenCover.color = Color.Lerp(transparentColor, solidColor, progress);
                progress -= 2f * Time.deltaTime;
                yield return null;
            }
        }
    }
}
