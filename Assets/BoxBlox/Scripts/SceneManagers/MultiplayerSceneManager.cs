﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Blox
{
    public class MultiplayerSceneManager : MonoBehaviour
    {
        float m_fTurnTimeInSeconds = 5f;
        uint m_uiNumberOfRounds = 3;
        uint m_uiNumberOfTurns = 6;
        uint m_uiNumberOfPowerUps = 3;
        float m_fMenuTimeInSeconds = 5f;

        Controllers.MultiplayerBoardController m_objBoard = null;
        Dictionary<bool, Controllers.MultiplayerHudController> m_dTurn = new Dictionary<bool, Controllers.MultiplayerHudController>() { };
        Dictionary<Types.PlayerID, bool> m_bPlayerReadyStatus = new Dictionary<Types.PlayerID, bool>();
        private bool m_bTurn;
        Models.MainMenuBoardModel m_objMainMenuBoardModel;
        Models.MultiGameSettingsUIModel m_objMultiGameSettingsUIModel;
        Models.MultiChosePowerUpsHUDModel m_objMultiChosePowerUpsHUDModel1;
        Models.MultiChosePowerUpsHUDModel m_objMultiChosePowerUpsHUDModel2;
        Models.BGHighlightsModel m_objBGHighlightsBlueModel;
        Models.BGHighlightsModel m_objBGHighlightsRedModel;
        Models.MultiAnnouncerModel m_objMultiAnnouncerModel;
        Models.MultiplayerHudModel m_objMultiplayerHudModel1;
        Models.MultiplayerHudModel m_objMultiplayerHudModel2;
        Models.MultiplayerBoardModel m_objBoardModel;
        Models.MultiGameEndUIModel m_objMultiGameEndUIModel;

        int m_iPlayerOneScore;
        int m_iPlayerTwoScore;

        private void Awake()
        {
            ShowGameSettingsUI();
        }

        public void OnPlayerScoreMessage(Messages.PlayerScoreMessage msg)
        {
            switch(msg.PlayerID)
            {
                case Types.PlayerID.One:
                    m_iPlayerOneScore = msg.Score;
                    break;

                case Types.PlayerID.Two:
                    m_iPlayerTwoScore = msg.Score;
                    break;

                default:
                    break;
            }
        }

        #region Game Settings
        private void ShowGameSettingsUI()
        {
            m_objMainMenuBoardModel = new Models.MainMenuBoardModel();
            CodeControl.Controller.Instantiate<Controllers.MainMenuBoardController>(Resources.Load("Prefabs/Boards/TitleBoard"), m_objMainMenuBoardModel); //** TODO: Change to correct resource folder.

            Messages.MultiplayerGameSettingsMessage.Register(OnMultiplayerGameSettingsMessage);
            m_objMultiGameSettingsUIModel = new Models.MultiGameSettingsUIModel();
            CodeControl.Controller.Instantiate<Controllers.MultiGameSettingsUIController>(Resources.Load("Prefabs/Settings/GameSettingsMenu"), m_objMultiGameSettingsUIModel); //** TODO: Change to correct resource folder.
            
            Messages.MilestoneMessage.Send(new Messages.MilestoneMessage(Types.MilestoneID.MultiGameSettings));
        }

        public void OnMultiplayerGameSettingsMessage(Messages.MultiplayerGameSettingsMessage msg)
        {
            Messages.MultiplayerGameSettingsMessage.Unregister(OnMultiplayerGameSettingsMessage);
            m_uiNumberOfRounds = msg.Rounds;
            m_uiNumberOfTurns = m_uiNumberOfRounds * 2;
            m_fTurnTimeInSeconds = msg.TurnTime;
            m_uiNumberOfPowerUps = msg.PowerCount;
            m_objMultiGameSettingsUIModel.Delete();

            if(m_uiNumberOfPowerUps > 0)
            {
                ShowPowerUpMenuUI();
            }
            else
            {
                ShowGameBoard();
            }
        }
        #endregion

        #region Power Up Menu
        private void ShowPowerUpMenuUI()
        {
            m_objMultiChosePowerUpsHUDModel1 = new Models.MultiChosePowerUpsHUDModel();
            m_objMultiChosePowerUpsHUDModel1.PlayerID = Types.PlayerID.One;
            m_objMultiChosePowerUpsHUDModel1.PlayerReady = false;
            m_objMultiChosePowerUpsHUDModel1.MaxPowerUps = m_uiNumberOfPowerUps;
            Controllers.MultiChosePowerUpsHUDController objCont1 = CodeControl.Controller.Instantiate<Controllers.MultiChosePowerUpsHUDController>(Resources.Load("Prefabs/UI/PlayerOneChoosePowerUpMenu"), m_objMultiChosePowerUpsHUDModel1);
            objCont1.SetPlayerReadyCallbacks(PlayerIsReadyControllerCallback, PlayerIsNotReadyControllerCallback);
            m_bPlayerReadyStatus[Types.PlayerID.One] = false;

            m_objMultiChosePowerUpsHUDModel2 = new Models.MultiChosePowerUpsHUDModel();
            m_objMultiChosePowerUpsHUDModel2.PlayerID = Types.PlayerID.Two;
            m_objMultiChosePowerUpsHUDModel2.PlayerReady = false;
            m_objMultiChosePowerUpsHUDModel2.MaxPowerUps = m_uiNumberOfPowerUps;
            Controllers.MultiChosePowerUpsHUDController objCont2 = CodeControl.Controller.Instantiate<Controllers.MultiChosePowerUpsHUDController>(Resources.Load("Prefabs/UI/PlayerTwoChoosePowerUpMenu"), m_objMultiChosePowerUpsHUDModel2);
            objCont2.SetPlayerReadyCallbacks(PlayerIsReadyControllerCallback, PlayerIsNotReadyControllerCallback);
            m_bPlayerReadyStatus[Types.PlayerID.Two] = false;

            Messages.MilestoneMessage.Send(new Messages.MilestoneMessage(Types.MilestoneID.MultiPowerSettings));
        }

        private bool HelperAreAllPlayersReady()
        {
            bool bPlayersAreReady = true;

            foreach (KeyValuePair<Types.PlayerID, bool> entry in m_bPlayerReadyStatus)
            {
                if (entry.Value == false)
                {
                    bPlayersAreReady = false;
                    break;
                }
            }

            return bPlayersAreReady;
        }

        public void PlayerIsReadyControllerCallback(Types.PlayerID eID)
        {
            m_bPlayerReadyStatus[eID] = true;

            if (HelperAreAllPlayersReady())
            {
                m_objMultiChosePowerUpsHUDModel1.Delete();
                m_objMultiChosePowerUpsHUDModel2.Delete();
                ShowGameBoard();
                //Messages.LoadLevelMessage.Send(new Messages.LoadLevelMessage(Types.LevelID.Multiplayer));
            }
        }

        public void PlayerIsNotReadyControllerCallback(Types.PlayerID eID)
        {
            m_bPlayerReadyStatus[eID] = false;
        }
        #endregion


        #region Game
        private void ShowGameBoard()
        {
            m_objMainMenuBoardModel.Delete();

            m_objMultiAnnouncerModel = new Models.MultiAnnouncerModel();
            CodeControl.Controller.Instantiate<Controllers.MultiAnnouncerController>(Resources.Load("Prefabs/Managers/Announcer"), m_objMultiAnnouncerModel); //** TODO: Change to correct resource folder.

            m_objBGHighlightsBlueModel = new Models.BGHighlightsModel();
            CodeControl.Controller.Instantiate<Controllers.BGHighlightsController>(Resources.Load("Prefabs/UI/BluePlayerGlow"), m_objBGHighlightsBlueModel);

            m_objBGHighlightsRedModel = new Models.BGHighlightsModel();
            CodeControl.Controller.Instantiate<Controllers.BGHighlightsController>(Resources.Load("Prefabs/UI/RedPlayerGlow"), m_objBGHighlightsRedModel);
            
            m_dTurn[false] = null;
            m_dTurn[true] = null;
            m_objBoardModel = new Models.MultiplayerBoardModel();
            int iRandomBoard = Random.Range(1, 7);
            m_objBoard = CodeControl.Controller.Instantiate<Controllers.MultiplayerBoardController>(Resources.Load("Prefabs/Boards/MultiplayerBoard" + iRandomBoard), m_objBoardModel);
            
            m_bTurn = true;
            Messages.GetPlayerMessage.Send(new Messages.GetPlayerMessage(Types.PlayerID.One, SetPlayerOne));
            Messages.GetPlayerMessage.Send(new Messages.GetPlayerMessage(Types.PlayerID.Two, SetPlayerTwo));

            Messages.MilestoneMessage.Send(new Messages.MilestoneMessage(Types.MilestoneID.MultiGameStart));
        }

        public void SetPlayerOne(Controllers.PlayerController objPlayer)
        {
            Messages.PlayerScoreMessage.Register(OnPlayerScoreMessage);
            //** TODO: Send Player Hud objPlayer info (like name). Store playerModel?
            m_objMultiplayerHudModel1 = new Models.MultiplayerHudModel();
            m_objMultiplayerHudModel1.turnTimeInSeconds = m_fTurnTimeInSeconds;
            m_objMultiplayerHudModel1.menuTimeInSeconds = m_fMenuTimeInSeconds;
            m_objMultiplayerHudModel1.PlayerID = Types.PlayerID.One;
            m_objMultiplayerHudModel1.roundsLeft = m_uiNumberOfRounds;
            Controllers.MultiplayerHudController m_objMultiplayerHudController = CodeControl.Controller.Instantiate<Controllers.MultiplayerHudController>(Resources.Load("Prefabs/UI/BluePlayerHUD"), m_objMultiplayerHudModel1);
            m_dTurn[true] = m_objMultiplayerHudController;
            StartIfReady();
        }

        public void SetPlayerTwo(Controllers.PlayerController objPlayer)
        {
            m_objMultiplayerHudModel2 = new Models.MultiplayerHudModel();
            m_objMultiplayerHudModel2.turnTimeInSeconds = m_fTurnTimeInSeconds;
            m_objMultiplayerHudModel2.menuTimeInSeconds = m_fMenuTimeInSeconds;
            m_objMultiplayerHudModel2.PlayerID = Types.PlayerID.Two;
            m_objMultiplayerHudModel2.roundsLeft = m_uiNumberOfRounds;
            Controllers.MultiplayerHudController m_objMultiplayerHudController = CodeControl.Controller.Instantiate<Controllers.MultiplayerHudController>(Resources.Load("Prefabs/UI/RedPlayerHUD"), m_objMultiplayerHudModel2);
            m_dTurn[false] = m_objMultiplayerHudController;
            StartIfReady();
        }

        private void StartIfReady()
        {
            if (m_dTurn[true] != null && m_dTurn[false] != null)
            {
                ShowPowerUpMenu();
                Messages.PlayerTurnMessage.Send(new Messages.PlayerTurnMessage(Types.PlayerID.One));
            }
        }

        public void CheckRounds()
        {
            m_uiNumberOfTurns--;

            if (m_uiNumberOfTurns == 0)
            {
                EndGame();
            }
            else
            {
                RotateBoard();
                SwapPlayerTurns();
            }
        }

        private void RotateBoard()
        {
            m_dTurn[m_bTurn].SetActive(false);
            m_dTurn[!m_bTurn].PerpareForNextTurn();
            m_objBoard.TransitionToNextPlayer(m_bTurn ? 0 : 1, OnRotateComplete);
        }
        private void SwapPlayerTurns()
        {
            m_bTurn = !m_bTurn;
            if (m_bTurn)
            {
                Messages.PlayerTurnMessage.Send(new Messages.PlayerTurnMessage(Types.PlayerID.One));
            }
            else
            {
                Messages.PlayerTurnMessage.Send(new Messages.PlayerTurnMessage(Types.PlayerID.Two));
            }
        }

        private void OnRotateComplete()
        {
            ShowPowerUpMenu();
        }

        private void ShowPowerUpMenu()
        {
            m_objBoard.SetActive(false); // Need to disable the grid so clicks (through the UI) do not register.
            // TODO: Just a note... this needs to be refactored. It's a boolean indicating whos turn it is. Then a dictionary with both players HUDs... Grosss.
            m_dTurn[m_bTurn].EnablePowerHUD(true, OnPowerSelectedCallback);
        }

        public void OnPowerSelectedCallback()
        {
            m_objBoard.SetActive(true);
            StartGame();
        }

        private void StartGame()
        {
            m_dTurn[m_bTurn].SetActive(true);
            m_dTurn[!m_bTurn].OtherPlayerIsActive();
            m_dTurn[m_bTurn].StartTurnTimer(CheckRounds);
        }

        private void EndGame()
        {
            Messages.MilestoneMessage.Send(new Messages.MilestoneMessage(Types.MilestoneID.MultiGameEnd));
            //** TODO: Find out who won, m_iPlayerOneScore or m_iPlayerTwoScore. Show Game Over UI.
            Messages.PlayerScoreMessage.Unregister(OnPlayerScoreMessage);
            ShowGameOver();

            Debug.LogError("Destroying Board");
            m_objMultiplayerHudModel1.Delete();
            m_objMultiplayerHudModel2.Delete();
            m_objBoardModel.Delete();
        }
        #endregion

        #region EndGame

        private void ShowGameOver()
        {
            m_objMultiGameEndUIModel = new Models.MultiGameEndUIModel();
            m_objMultiGameEndUIModel.PlayerOneScore = m_iPlayerOneScore;
            m_objMultiGameEndUIModel.PlayerTwoScore = m_iPlayerTwoScore;
            Controllers.MultiGameEndUIController objMultiGameEndUIController = CodeControl.Controller.Instantiate<Controllers.MultiGameEndUIController>(Resources.Load("Prefabs/UI/GameEndingUI"), m_objMultiGameEndUIModel);
            objMultiGameEndUIController.StartGameOverSequence(OnGameOverFinishedCallback);
        }

        public void OnGameOverFinishedCallback()
        {
            //** TODO: This is not being called. Need to call it so it cleans up. 
            m_objBGHighlightsBlueModel.Delete();
            m_objBGHighlightsRedModel.Delete();

            m_objMultiGameEndUIModel.Delete();
            m_objMultiAnnouncerModel.Delete();
            Messages.LoadLevelMessage.Send(new Messages.LoadLevelMessage(Types.LevelID.MainMenu));
        }
        #endregion
    }


}
