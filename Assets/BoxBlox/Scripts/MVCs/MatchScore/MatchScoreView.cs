using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Blox.Views
{
    public class MatchScoreView : MonoBehaviour
    {
        public Vector3 startPos;
        public float fMoveDistance;
        public float fTimeToGetThere;
        private Text m_objScoreText = null;
        private System.Action m__funcOnFinishedCallback = null;
        private string m_strMoveTweenName;
        private string m_strFadeTweenName;

        void Awake()
        {
        }

        private Text GetText()
        {
            if(m_objScoreText == null)
            {
                m_objScoreText = GetComponent<Text>();
            }

            return m_objScoreText;
        }

        // Update is called once per frame
        void Update()
        {
        }
        public void SetOnFinishedCallback(System.Action funcOnFinished)
        {
            m__funcOnFinishedCallback = funcOnFinished;
        }

        public void ShowScoreText(string strInfo)
        {
            Vector3 v3StartPos = new Vector3(Screen.width * startPos.x, Screen.height * startPos.y, 0);
            gameObject.transform.position = v3StartPos;
            
            //** Do stuff. 
            GetText().text = strInfo;
            m_strMoveTweenName = fMoveDistance.ToString() + strInfo + Time.time + "Move";
            iTween.MoveBy(gameObject, iTween.Hash("name", m_strMoveTweenName, "y", Screen.height * fMoveDistance, "time", fTimeToGetThere, "oncompletetarget", gameObject, "oncomplete", "OnMoveTweenComplete"));

            m_strFadeTweenName = fMoveDistance.ToString() + strInfo + Time.time + "Fade";
            iTween.FadeTo(gameObject, iTween.Hash("name", m_strFadeTweenName, "alpha", 1f, "time", fTimeToGetThere, "oncompletetarget", gameObject, "oncomplete", "OnFadeTweenComplete"));
        }

        public void OnMoveTweenComplete()
        {
            m_strMoveTweenName = "";

            if(m_strFadeTweenName == "")
            {
                m__funcOnFinishedCallback();
            }
        }

        public void OnFadeTweenComplete()
        {
            m_strFadeTweenName = "";

            if (m_strMoveTweenName == "")
            {
                m__funcOnFinishedCallback();
            }
        }

        void OnDestroy()
        {
            if (m_strMoveTweenName != "")
                iTween.StopByName(m_strMoveTweenName);

            if (m_strFadeTweenName != "")
                iTween.StopByName(m_strFadeTweenName);
        }

    }
}
