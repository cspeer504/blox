using UnityEngine;
using System.Collections;

namespace Blox.Controllers
{
    public class MatchScoreController : CodeControl.Controller<Models.MatchScoreModel>
    {

        private Views.MatchScoreView view;
				
        protected override void OnInitialize()
        {
            view = GetComponent<Views.MatchScoreView>();
			
            if(view == null)
            {
                Debug.LogError("MatchScoreController could not find associated view component: MatchScoreView");
            }
            view.SetOnFinishedCallback(ScoreShown);

        }

        public void ShowScoreText()
        {
            //string strScoreText = "+ " + model.score + "\n" + model.strExtraInfo;
            string strScoreText = model.strExtraInfo;
            view.ShowScoreText(strScoreText);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }
        public void ScoreShown()
        {
            model.Delete();
        }


        protected override void OnModelChanged()
        {
        }
    }
}
