using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Blox.Views
{
    public class MultiAnnouncerView : MonoBehaviour
    {
        public AudioSource countdownThree;
        public AudioSource countdownTwo;
        public AudioSource countdownOne;
        public AudioSource countdownGo;
        public AudioSource bigWinNice;
        public AudioSource bigWinSweet;
        public AudioSource bigWinAwesome;
        public AudioSource bigWinBonus;
        public AudioSource bigWinInsane;
        public AudioSource winnerIs;
        public AudioSource winnerRed;
        public AudioSource winnerBlue;
        public AudioSource winnerCongrats;

        public void PlayCountdownThree() { countdownThree.Play(); }
        public void PlayCountdownTwo()   { countdownTwo.Play(); }
        public void PlayCountdownOne()   { countdownOne.Play(); }
        public void PlayCountdownGo()    { countdownGo.Play();  }
        public void PlayBigWinNice()     { bigWinNice.Play();   }        
        public void PlayBigWinSweet()    { bigWinSweet.Play();  }
        public void PlayBigWinAwesome()  { bigWinAwesome.Play(); }
        public void PlayBigWinBonus()    { bigWinBonus.Play();   }
        public void PlayBigWinInsane()   { bigWinInsane.Play();  }
        public void PlayWinnerIs()       { winnerIs.Play();   }
        public void PlayWinnerRed()      { winnerRed.Play(); }
        public void PlayWinnerBlue()     { winnerBlue.Play(); }
        public void PlayWinnerCongrats() { winnerCongrats.Play(); }


        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
        }
    }
}
