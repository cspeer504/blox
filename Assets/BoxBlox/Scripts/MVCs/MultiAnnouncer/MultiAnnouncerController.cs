using UnityEngine;
using System.Collections;

namespace Blox.Controllers
{
    public class MultiAnnouncerController : CodeControl.Controller<Models.MultiAnnouncerModel>
    {
        bool m_bSuppress = false;

        private Views.MultiAnnouncerView view;
				
		/*  // To Instantiate me, use one of the following:
            Models.MultiAnnouncerModel objMultiAnnouncerModel = new Models.MultiAnnouncerModel();
			Controllers.MultiAnnouncerController m_objMultiAnnouncerController = Controller.Instantiate<Controllers.MultiAnnouncerController>(objMultiAnnouncerModel); //** Create on a new (Empty) Game Object
            Controllers.MultiAnnouncerController m_objMultiAnnouncerController = Controller.Instantiate<Controllers.MultiAnnouncerController>(Resources.Load("PathToPrefabInAnyResourceFolder"), objMultiAnnouncerModel); //** TODO: Change to correct resource folder.
		*/

        protected override void OnInitialize()
        {
            view = GetComponent<Views.MultiAnnouncerView>();
			
            if(view == null)
            {
                Debug.LogError("MultiAnnouncerController could not find associated view component: MultiAnnouncerView");
            }

            Messages.CountdownMessage.Register(OnCountdownMessage);
            Messages.BigWinMessage.Register(OnBigWinMessage);
            Messages.WinnerMessage.Register(OnWinnerMessage);
            Messages.PlayerTurnMessage.Register(OnPlayerTurnMessage);
            Messages.BoardActionMessage.Register(OnBoardActionMessage);
        }


        protected override void OnDestroy()
        {
            Messages.CountdownMessage.Unregister(OnCountdownMessage);
            Messages.BigWinMessage.Unregister(OnBigWinMessage);
            Messages.WinnerMessage.Unregister(OnWinnerMessage);
            Messages.PlayerTurnMessage.Unregister(OnPlayerTurnMessage);
            Messages.BoardActionMessage.Unregister(OnBoardActionMessage);

            base.OnDestroy();
        }

        protected override void OnModelChanged()
        {
        }
        public void OnBoardActionMessage(Messages.BoardActionMessage msg)
        {
            switch(msg.action)
            {
                case Types.BoardAction.Colorblind:
                    m_bSuppress = true;
                    break;
                default:
                    m_bSuppress = false;
                    break;

            }    
        }

        public void OnPlayerTurnMessage(Messages.PlayerTurnMessage msg)
        {
            switch(msg.PlayerType)
            {
                default:
                    m_bSuppress = false;
                    break;
            }
        }

        public void OnCountdownMessage(Messages.CountdownMessage msg)
        {

            switch(msg.Type)
            {
                case Types.CountdownType.CountdownOne:
                    view.PlayCountdownOne();
                    break;
                case Types.CountdownType.CountdownTwo:
                    view.PlayCountdownTwo();
                    break;
                case Types.CountdownType.CountdownThree:
                    view.PlayCountdownThree();
                    break;
                case Types.CountdownType.CountdownGo:
                    view.PlayCountdownGo();
                    break;
                default:
                    break;

            }
        }

        public void OnBigWinMessage(Messages.BigWinMessage msg)
        {
            if (m_bSuppress)
                return;

            switch (msg.Type)
            {
                case Types.BigWinType.Nice:
                    view.PlayBigWinNice();
                    break;
                case Types.BigWinType.Sweet:
                    view.PlayBigWinSweet();
                    break;
                case Types.BigWinType.Awesome:
                    view.PlayBigWinAwesome();
                    break;
                case Types.BigWinType.Bonus:
                    view.PlayBigWinBonus();
                    break;
                case Types.BigWinType.Insane:
                    view.PlayBigWinInsane();
                    break;
                default:
                    break;

            }
        }

        public void OnWinnerMessage(Messages.WinnerMessage msg)
        {
            switch(msg.Callout)
            {
                case Types.WinnerCallout.WinnerIs:
                    view.PlayWinnerIs();
                    break;
                case Types.WinnerCallout.WinnerCongrats:
                    view.PlayWinnerCongrats();
                    break;
                case Types.WinnerCallout.WinnerBluePlayer:
                    view.PlayWinnerBlue();
                    break;
                case Types.WinnerCallout.WinnerRedPlayer:
                    view.PlayWinnerRed();
                    break;
                default:
                    break;

            }
        }
    }
}
