﻿using UnityEngine;
using System.Collections;

namespace Blox.Controllers
{
    public class MultiplayerBoardController : CodeControl.Controller<Models.MultiplayerBoardModel>
    {

        private Views.MultiplayerBoardView m_objView;

        protected override void OnInitialize()
        {
            m_objView = GetComponent<Views.MultiplayerBoardView>();
            Messages.BoardActionMessage.Register(OnBoardActionMessage);
            Messages.MatchMessage.Register(OnMatchMessage);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            Messages.BoardActionMessage.Unregister(OnBoardActionMessage);
            Messages.MatchMessage.Unregister(OnMatchMessage);
        }

        public void SetActive(bool bFlag)
        {
            m_objView.SetActive(bFlag);
        }

        public void TransitionToNextPlayer(int iPlayerIndex, System.Action funcCallback)
        {
            m_objView.RotateBoard(iPlayerIndex, funcCallback);
        }

        public void OnMatchMessage(Messages.MatchMessage msg)
        {
            int iCount = msg.count;

            if(iCount >= 9 && iCount <= 10)
            {
                Messages.BigWinMessage.Send(new Messages.BigWinMessage(Types.BigWinType.Nice));
            }
            else if (iCount >= 11 && iCount <= 13)
            {
                Messages.BigWinMessage.Send(new Messages.BigWinMessage(Types.BigWinType.Sweet));
            }
            else if (iCount >= 13 && iCount <= 17)
            {
                Messages.BigWinMessage.Send(new Messages.BigWinMessage(Types.BigWinType.Awesome));
            }
            else if (iCount >= 18 && iCount <= 22)
            {
                Messages.BigWinMessage.Send(new Messages.BigWinMessage(Types.BigWinType.Bonus));
            }
            else if (iCount >= 23)
            {
                Messages.BigWinMessage.Send(new Messages.BigWinMessage(Types.BigWinType.Insane));
            }
        }

        public void OnBoardActionMessage(Messages.BoardActionMessage objMessage)
        {
            switch(objMessage.action)
            {
                case Types.BoardAction.None:
                    break;

                case Types.BoardAction.Randomize:
                    m_objView.ActionRandomizeBoard();
                    break;

                case Types.BoardAction.Colorblind:
                    m_objView.ActionColorblind();
                    break;

                case Types.BoardAction.TimeBoost:
                    Blox.Messages.AddRoundTimeMessage.Send(new Blox.Messages.AddRoundTimeMessage(0, 0.25f));
                    break;

                case Types.BoardAction.ColorBoost:
                    m_objView.ActionColorBoost();
                    break;

                case Types.BoardAction.QuantityBoost:
                    m_objView.ActionQuantityBoost();
                    break;

                case Types.BoardAction.Clear:
                    m_objView.ActionClearActions();
                    break;

                default:
                    Debug.LogWarning("Board Action not recognized: " + objMessage.action);
                    break;
            }
        }
        
    }

}