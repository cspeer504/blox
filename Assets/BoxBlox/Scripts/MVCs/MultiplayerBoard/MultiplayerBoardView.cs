﻿using UnityEngine;
using System.Collections;

namespace Blox.Views
{
    public class MultiplayerBoardView : MonoBehaviour
    {
        public float[] playerRotation;
        public Gamelogic.Grids.TileCell[] cellPrefabs;
        public GameObject cover;

        MatchGrid m_objGrid = null;
        System.Action m_funcFinishedRotationCallback;
        ArrayList m_aActiveActions = new ArrayList();
        private string m_strTweenName = "";

        void Start()
        {
            UnityEngine.Canvas objCanvas = gameObject.GetComponent<Canvas>();
            objCanvas.worldCamera = Camera.main;
            m_strTweenName = gameObject.name + "Rotation";
            //SetGridCell(-1);
        }

        void OnDestroy()
        {
            iTween.StopByName(m_strTweenName);
            m_objGrid.StopAll();
        }

        private MatchGrid GetGrid()
        {
            if (m_objGrid == null)
            {
                m_objGrid = gameObject.GetComponent<MatchGrid>();

                Debug.Assert(m_objGrid != null, "MultiplayerBoardView is not attached to a GO with MatchGrid script!");
            }
            return m_objGrid;
        }

        public void SetGridCell(int iCellIndex)
        {
            int iCellIndexToUse = iCellIndex;
            if(iCellIndexToUse < 0)
            {
                iCellIndexToUse = Random.Range(0, cellPrefabs.Length);
            }
            else if(iCellIndexToUse >= cellPrefabs.Length)
            {
                iCellIndexToUse = cellPrefabs.Length - 1;
                Debug.LogWarning("SetGridCell: Cell Index is larger than the last of the list. Setting to " + iCellIndexToUse);
            }

            if(iCellIndexToUse >= 0)
            {
                m_objGrid.GridBuilder.CellPrefab = cellPrefabs[iCellIndexToUse];
                m_objGrid.InitGrid();
            }

        }

        public void SetActive(bool bFlag)
        {
            
            if(cover != null)
            {
                cover.SetActive(!bFlag);
            }
            
            GetGrid().SetActive(bFlag);
        }

        public void RotateBoard(int iPlayerIndex, System.Action funcCallback)
        {
            //** TODO: Remove player index. Just use next available rotation or pass rotation amount in. 
            if (iPlayerIndex < playerRotation.Length)
            {
                m_funcFinishedRotationCallback = funcCallback;
                SetActive(false);
                iTween.RotateTo(gameObject, iTween.Hash("name", m_strTweenName, "z", playerRotation[iPlayerIndex], "time", 2, "oncompletetarget", gameObject, "oncomplete", "OnRotateComplete"));
            }
            else
            {
                Debug.LogError("Could not rotate the board. No playerRotation definition for (player) index " + iPlayerIndex);
            }
        }

        public void ActionClearActions()
        {
            m_aActiveActions.Clear();
            GetGrid().SetMatchRule(Types.MatchRule.SameNodeType);
            GetGrid().SetMatchType(Types.MatchType.Normal);
        }

        public void ActionColorblind()
        {
            m_aActiveActions.Add(Types.BoardAction.Colorblind);
            GetGrid().SetMatchRule(Types.MatchRule.SurroundingNodes);
        }

        public void ActionColorBoost()
        {
            m_aActiveActions.Add(Types.BoardAction.ColorBoost);
            ArrayList aTypes = new ArrayList() { Types.MatchType.ColorBoostRed25 , Types.MatchType.ColorBoostBlue25,
                                                 Types.MatchType.ColorBoostGreen25, Types.MatchType.ColorBoostYellow25 };
            int iRandom = Random.Range(0, aTypes.Count);            
            GetGrid().SetMatchType((Types.MatchType) aTypes[iRandom]);
        }

        public void ActionQuantityBoost()
        {
            m_aActiveActions.Add(Types.BoardAction.QuantityBoost);
            GetGrid().SetMatchType(Types.MatchType.QuantityBoost1);
        }

        public void ActionRandomizeBoard()
        {
            if(!m_aActiveActions.Contains(Types.BoardAction.Randomize))
            {
                m_aActiveActions.Add(Types.BoardAction.Randomize);
                GetGrid().RandomizeGridInstantly();
                m_aActiveActions.Remove(Types.BoardAction.Randomize);
            }
            else
            {
                Debug.LogWarning("Board is already randomizing. Ignoring second call.");
            }
        }


        private void OnRotateComplete()
        {
            SetActive(true);
            m_funcFinishedRotationCallback();
        }

        // Update is called once per frame
        void Update()
        {
        }
    }
}