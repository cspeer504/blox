using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Blox.Views
{
    public class MainMenuBoardView : MonoBehaviour
    {
        MatchGrid m_objGrid = null;
        void Start()
        {
        }


        private MatchGrid GetGrid()
        {
            if (m_objGrid == null)
            {
                m_objGrid = gameObject.GetComponent<MatchGrid>();

                //Debug.Assert(m_objGrid != null, "MultiplayerBoardView is not attached to a GO with MatchGrid script!");
            }
            return m_objGrid;
        }

        // Update is called once per frame
        void Update()
        {
        }

        public void RemoveRandomBottomCell()
        {
            MatchGrid objGrid = GetGrid();

            if(objGrid != null)
            {
                int iRandomCol = Random.Range(0, objGrid.GetWidth());
                int iRow = 0;//objGrid.GetHeight() - 1;
                
                Gamelogic.Grids.RectPoint pt = new Gamelogic.Grids.RectPoint(iRandomCol, iRow);
                objGrid.RemoveCell(pt);
            }
        }

    }
}
