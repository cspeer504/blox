using UnityEngine;
using System.Collections;

namespace Blox.Controllers
{
    public class MainMenuBoardController : CodeControl.Controller<Models.MainMenuBoardModel>
    {

        public float minFallTime = 0.1f;
        public float maxFallTime = 0.75f;
        private Views.MainMenuBoardView view;
				
		/*  // To Instantiate me, use one of the following:
            Models.MainMenuBoardModel objMainMenuBoardModel = new Models.MainMenuBoardModel();
			Controllers.MainMenuBoardController m_objMainMenuBoardController = Controller.Instantiate<Controllers.MainMenuBoardController>(objMainMenuBoardModel); //** Create on a new (Empty) Game Object
            Controllers.MainMenuBoardController m_objMainMenuBoardController = Controller.Instantiate<Controllers.MainMenuBoardController>(Resources.Load("PathToPrefabInAnyResourceFolder"), objMainMenuBoardModel); //** TODO: Change to correct resource folder.
		*/

        protected override void OnInitialize()
        {
            view = GetComponent<Views.MainMenuBoardView>();
			
            if(view == null)
            {
                Debug.LogError("MainMenuBoardController could not find associated view component: MainMenuBoardView");
            }
            StartCoroutine("StartGridAnimation");
        }

        protected override void OnModelChanged()
        {
        }

        private IEnumerator StartGridAnimation()
        {
            while (true)
            {
                yield return new WaitForSeconds(Random.Range(minFallTime, maxFallTime));
                view.RemoveRandomBottomCell();
            }
        }
    }
}
