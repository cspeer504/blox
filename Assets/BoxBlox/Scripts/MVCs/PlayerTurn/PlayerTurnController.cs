using UnityEngine;
using System.Collections;

namespace Blox.Controllers
{
    public class PlayerTurnController : CodeControl.Controller<Models.PlayerTurnModel>
    {

        private Views.PlayerTurnView view;
				
		/*  // To Instantiate me, use one of the following:
            Models.PlayerTurnModel objPlayerTurnModel = new Models.PlayerTurnModel();
			Controllers.PlayerTurnController m_objPlayerTurnController = Controller.Instantiate<Controllers.PlayerTurnController>(objPlayerTurnModel); //** Create on a new (Empty) Game Object
            Controllers.PlayerTurnController m_objPlayerTurnController = Controller.Instantiate<Controllers.PlayerTurnController>(Resources.Load("PathToPrefabInAnyResourceFolder"), objPlayerTurnModel); //** TODO: Change to correct resource folder.
		*/

        protected override void OnInitialize()
        {
            view = GetComponent<Views.PlayerTurnView>();
			
            if(view == null)
            {
                Debug.LogError("PlayerTurnController could not find associated view component: PlayerTurnView");
            }
        }

        protected override void OnModelChanged()
        {
        }
    }
}
