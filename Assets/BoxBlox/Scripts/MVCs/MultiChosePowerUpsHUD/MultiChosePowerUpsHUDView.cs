using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Blox.Views
{
    public class MultiChosePowerUpsHUDView : MonoBehaviour
    {
        public Types.PowerUp.Type[] typeList;
        public UnityEngine.UI.Button[] buttonList;
        public AudioSource powerUpButtonEnable;
        public AudioSource powerUpButtonDisable;
        public UnityEngine.UI.Button playerReadyButton;
        public AudioSource playerReadyAudio;
        public UnityEngine.UI.Button playerCancelButton;
        public AudioSource playerCancelAudio;
        public UnityEngine.UI.Text descriptionText;
        
        private struct PowerUpButtonInfo
        {
            public Types.PowerUp.Type ePowerUpType;
            public UnityEngine.UI.Button objButton;
        }
        
        Vector3 m_v3ButtonPos;
        ArrayList m_aAvailablePowerUps;
        Types.PowerUp m_objNullType;
        System.Action<Types.PowerUp.Type> m_funcPowerUpToggledCallback;
        System.Action<bool> m_funcPlayerRequestReadyCallback;
        System.Action m_funcNoButtonSelectedCallback;
        Color m_objNormalButtonColor = Color.white;

        public System.Action FuncNoButtonSelectedCallback
        {
            get { return m_funcNoButtonSelectedCallback; }
            set { m_funcNoButtonSelectedCallback = value; }
        }

        void Start()
        {
            m_objNullType = new Types.PowerUp();
            m_objNullType.eID = Types.PowerUp.Type.None;
            
            if(playerCancelButton != null) // Cancel button is Optional
                HelperEnableButton(playerCancelButton, false);
            
            if (buttonList.Length == typeList.Length)
            {
                for (uint i = 0; i < buttonList.Length; ++i)
                {
                    m_objNormalButtonColor = buttonList[i].image.color;
                    HelperSetPowerUpButtonTriggers(buttonList[i], typeList[i]);
                    HelperSetButtonText(buttonList[i], HelperGetPowerUpInfo(typeList[i]).strName);
                }
            }
            else
            {
                Debug.LogError("Button List size (" + buttonList.Length + ") must equal Type List size (" + typeList.Length + ")!");
            }

            HelperSetButtonTrigger(playerReadyButton, EventTriggerType.PointerClick, OnButtonPressReady);

            if (playerCancelButton != null) // Cancel button is Optional
                HelperSetButtonTrigger(playerCancelButton, EventTriggerType.PointerClick, OnButtonPressCancel);

        }
        
        void Update() { }
        
        public void SetActionFeedback(string strFeedback)
        {
            if(descriptionText != null) // Description text field is optional
                descriptionText.text = strFeedback;
        }

        public void SetPlayerIsReady(bool bFlag)
        {
            for (uint i = 0; i < buttonList.Length; ++i)
            {
                buttonList[i].interactable = !bFlag;
            }

            if (playerCancelButton != null) // Cancel button is Optional
            {
                if (bFlag)
                {
                    playerCancelAudio.Play();
                    HelperSwapButtons(playerReadyButton, playerCancelButton);
                }
                else
                {
                    playerReadyAudio.Play();
                    HelperSwapButtons(playerCancelButton, playerReadyButton);
                }
            }
        }

        public void SetAvailablePowerUps(ArrayList aAvailablePowerUps)
        {
            m_aAvailablePowerUps = aAvailablePowerUps;
        }

        public void PlayerSelectedPower(Types.PowerUp.Type ePowerID)
        {
            powerUpButtonEnable.Play();
            HelperSetPowerUpButton(ePowerID, true);
        }

        public void PlayerDeselectedPower(Types.PowerUp.Type ePowerID)
        {
            powerUpButtonDisable.Play();
            HelperSetPowerUpButton(ePowerID, false);
        }


        #region Controller Callbacks
        public void SetPowerUpToggleCallback(System.Action<Types.PowerUp.Type> funcCallback)
        {
            m_funcPowerUpToggledCallback = funcCallback;
        }
        public void SetPlayerRequestReadyCallback(System.Action<bool> funcCallback)
        {
            m_funcPlayerRequestReadyCallback = funcCallback;
        }
        #endregion

        #region Button Callbacks
        private void OnPowerUpButtonMouseEnter(PowerUpButtonInfo objInfo) { HelperMouseEnter(objInfo.ePowerUpType); }
        private void OnPowerUpButtonMouseExit(PowerUpButtonInfo objInfo) { FuncNoButtonSelectedCallback(); }
        private void OnPowerUpButtonClicked(PowerUpButtonInfo objInfo) { HelperOnButtonClicked(objInfo); }
        
        private void OnButtonPressReady() { m_funcPlayerRequestReadyCallback(true); }
        private void OnButtonPressCancel() { m_funcPlayerRequestReadyCallback(false); }
        #endregion

        #region Helper Functions

        private void HelperSetButtonText(UnityEngine.UI.Button objButton, string strText)
        {
            UnityEngine.UI.Text objText = objButton.gameObject.GetComponentInChildren<UnityEngine.UI.Text>();
            
            if (objText != null)
            {
                objText.text = strText;
            }
        }

        private void HelperOnButtonClicked(PowerUpButtonInfo objInfo)
        {
            if (objInfo.objButton.IsInteractable())
            {
                m_funcPowerUpToggledCallback(objInfo.ePowerUpType);
            }
        }

        private void HelperSetPowerUpButtonTriggers(UnityEngine.UI.Button objButton, Types.PowerUp.Type ePowerUpType)
        {
            PowerUpButtonInfo objButtonInfo; objButtonInfo.objButton = objButton; objButtonInfo.ePowerUpType = ePowerUpType;
            HelperSetPowerUpButtonTrigger(objButtonInfo, EventTriggerType.PointerEnter, OnPowerUpButtonMouseEnter);
            HelperSetPowerUpButtonTrigger(objButtonInfo, EventTriggerType.PointerExit, OnPowerUpButtonMouseExit);
            HelperSetPowerUpButtonTrigger(objButtonInfo, EventTriggerType.PointerClick, OnPowerUpButtonClicked);
        }

        private void HelperSetPowerUpButtonTrigger(PowerUpButtonInfo objButtonInfo, EventTriggerType eTriggerType, System.Action<PowerUpButtonInfo> funcCallback)
        {
            EventTrigger trigger = objButtonInfo.objButton.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = eTriggerType;
            entry.callback.AddListener((eventData) => { funcCallback(objButtonInfo); });
            trigger.triggers.Add(entry);
        }

        private void HelperSetButtonTrigger(UnityEngine.UI.Button objButton, EventTriggerType eTriggerType, System.Action funcCallback)
        {
            EventTrigger trigger = objButton.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = eTriggerType;
            entry.callback.AddListener((eventData) => { funcCallback(); });
            trigger.triggers.Add(entry);
        }


        private void HelperSwapButtons(UnityEngine.UI.Button objButtonOff, UnityEngine.UI.Button objButtonOn)
        {
            m_v3ButtonPos = objButtonOff.transform.position;
            HelperEnableButton(objButtonOn, true);
            HelperEnableButton(objButtonOff, false);
        }

        private void HelperEnableButton(UnityEngine.UI.Button objButton, bool bFlag)
        {
            objButton.enabled = bFlag;
            objButton.GetComponentInChildren<CanvasRenderer>().SetAlpha(bFlag ? 1 : 0);
            objButton.GetComponentInChildren<UnityEngine.UI.Text>().color = bFlag ? Color.white : Color.clear;
            objButton.transform.position = bFlag ? m_v3ButtonPos : new Vector3(-9999, -9999, 0);
        }

        private void HelperMouseEnter(Types.PowerUp.Type ePower)
        {
            Types.PowerUp objPower = HelperGetPowerUpInfo(ePower);
            SetActionFeedback("Power Up: " + objPower.eID.ToString() + "\n---------------------------------------\n\n" + objPower.strDescription + "\n\n" + objPower.strTips);
        }

        private Types.PowerUp HelperGetPowerUpInfo(Types.PowerUp.Type eID)
        {
            Types.PowerUp objPowerUpReturn = m_objNullType;
            foreach (Types.PowerUp objPowerUp in m_aAvailablePowerUps)
            {
                if (objPowerUp.eID == eID)
                {
                    objPowerUpReturn = objPowerUp;
                    break;
                }
            }

            if (objPowerUpReturn.eID == Types.PowerUp.Type.None)
            {
                Debug.LogWarning("Could not find available power up of type " + eID.ToString());
            }

            return objPowerUpReturn;
        }

        private void HelperTogglePowerUpButton(UnityEngine.UI.Button objButton)
        {
            if (objButton.image.color == m_objNormalButtonColor)
            {
                objButton.image.color = Color.green;
            }
            else
            {
                objButton.image.color = m_objNormalButtonColor;
            }
        }

        private void HelperSetPowerUpButton(Types.PowerUp.Type ePowerID, bool bActive)
        {
            for (uint i = 0; i < typeList.Length; ++i)
            {
                if(typeList[i] == ePowerID)
                {
                    if (bActive)
                    {
                        buttonList[i].image.color = Color.green;
                    }
                    else
                    {
                        buttonList[i].image.color = m_objNormalButtonColor;
                    }
                }
            }
        }
        #endregion
    }
}
