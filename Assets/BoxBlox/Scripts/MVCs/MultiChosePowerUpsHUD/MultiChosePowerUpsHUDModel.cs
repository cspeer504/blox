using UnityEngine;
using System.Collections;

namespace Blox.Models
{
    public class MultiChosePowerUpsHUDModel : CodeControl.Model
    {
        Types.PlayerID ePlayerID;
        public Types.PlayerID PlayerID
        {
            get { return ePlayerID; }
            set { ePlayerID = value; }
        }

        bool bPlayerReady;
        public bool PlayerReady
        {
            get { return bPlayerReady; }
            set { bPlayerReady = value; }
        }
        
        uint uiMaxPowerUps;
        public uint MaxPowerUps
        {
            get { return uiMaxPowerUps; }
            set { uiMaxPowerUps = value; }
        }
    }
}
