using UnityEngine;
using System.Collections;

namespace Blox.Controllers
{
    public class MultiChosePowerUpsHUDController : CodeControl.Controller<Models.MultiChosePowerUpsHUDModel>
    {

        private Views.MultiChosePowerUpsHUDView view;

        /*  // To Instantiate me, use one of the following:
            Models.MultiChosePowerUpsHUDModel objMultiChosePowerUpsHUDModel = new Models.MultiChosePowerUpsHUDModel();
			Controllers.MultiChosePowerUpsHUDController m_objMultiChosePowerUpsHUDController = Controller.Instantiate<Controllers.MultiChosePowerUpsHUDController>(objMultiChosePowerUpsHUDModel); //** Create on a new (Empty) Game Object
            Controllers.MultiChosePowerUpsHUDController m_objMultiChosePowerUpsHUDController = Controller.Instantiate<Controllers.MultiChosePowerUpsHUDController>(Resources.Load("PathToPrefabInAnyResourceFolder"), objMultiChosePowerUpsHUDModel); //** TODO: Change to correct resource folder.
		*/
        Controllers.PlayerController m_objPlayer = null;
        private System.Action<Types.PlayerID> m_funcPlayerIsReadyCallback;
        private System.Action<Types.PlayerID> m_funcPlayerIsNotReadyCallback;
        bool m_bPlayerIsReady = false;

        protected override void OnInitialize()
        {
            view = GetComponent<Views.MultiChosePowerUpsHUDView>();
			
            if(view == null)
            {
                Debug.LogError("MultiChosePowerUpsHUDController could not find associated view component: MultiChosePowerUpsHUDView");
            }
            Messages.GetPlayerMessage.Send(new Messages.GetPlayerMessage(model.PlayerID, SetPlayer));
            model.PlayerReady = false;

            view.SetPowerUpToggleCallback(OnTogglePowerViewCallback);
            view.SetPlayerRequestReadyCallback(OnPlayerRequestingReady);
            SetActionFeedbackToPowersLeft();
            view.FuncNoButtonSelectedCallback = SetActionFeedbackToPowersLeft;
        }
        
        public void SetPlayerReadyCallbacks(System.Action<Types.PlayerID> funcOnReadyCallback, System.Action<Types.PlayerID> funcOnNotReadyCallback)
        {
            m_funcPlayerIsReadyCallback = funcOnReadyCallback;
            m_funcPlayerIsNotReadyCallback = funcOnNotReadyCallback;
        }

        public void SetPlayer(Controllers.PlayerController objPlayer)
        {
            m_objPlayer = objPlayer;
            m_objPlayer.removeAllPowers();
            view.SetAvailablePowerUps(m_objPlayer.getPowerUpTemplates());
        }

        public void SetActionFeedbackToPowersLeft()
        {
            uint uiPowerCount = m_objPlayer.hasHowManyPowers();
            uint uiLeft = model.MaxPowerUps - uiPowerCount;

            if(uiLeft == 0)
            {
                if(m_bPlayerIsReady)
                {
                    view.SetActionFeedback("Please wait for the other player...");
                }
                else
                {
                    view.SetActionFeedback("Press READY to begin!");
                }
            }
            else if (uiLeft == 1)
            {
                view.SetActionFeedback("Pick " + uiLeft + " more Power Up!");
            }
            else if(uiLeft > 1)
            {
                view.SetActionFeedback("Pick " + uiLeft + " Power Ups!");
            }
        }

        protected override void OnModelChanged()
        {
        }

        public void OnPlayerRequestingReady(bool bFlag)
        {
            if(bFlag)
            {
                uint uiPowerCount = m_objPlayer.hasHowManyPowers();
                if (uiPowerCount >= model.MaxPowerUps)
                {
                    view.SetPlayerIsReady(true);
                    m_funcPlayerIsReadyCallback(model.PlayerID);
                    m_bPlayerIsReady = true;
                    SetActionFeedbackToPowersLeft();
                }
                else
                {
                    uint uiLeft = model.MaxPowerUps - uiPowerCount;
                    view.SetActionFeedback("Choose " + uiLeft + " more powers!");
                }
            }
            else
            {
                view.SetPlayerIsReady(false);
                m_funcPlayerIsNotReadyCallback(model.PlayerID);
                m_bPlayerIsReady = false;
                SetActionFeedbackToPowersLeft();
            }
        }

        public void OnTogglePowerViewCallback(Types.PowerUp.Type ePower)
        {
            if (m_objPlayer.hasPower(ePower) > 0)
            {
                m_objPlayer.removePower(ePower);
                view.PlayerDeselectedPower(ePower);
            }
            else
            {
                uint uiPowerCount = m_objPlayer.hasHowManyPowers();
                if (uiPowerCount < model.MaxPowerUps)
                {
                    m_objPlayer.addPower(ePower);
                    view.PlayerSelectedPower(ePower);
                }
                else
                {
                    view.SetActionFeedback("You can only have " + model.MaxPowerUps + " power ups! Remove one to add another.");
                }
            }
        }
    }
}
