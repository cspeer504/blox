using UnityEngine;
using System.Collections;

namespace Blox.Controllers
{
    public class MultiGameEndUIController : CodeControl.Controller<Models.MultiGameEndUIModel>
    {

        private Views.MultiGameEndUIView view;

        private System.Action m_funcCallback;
        protected override void OnInitialize()
        {
            view = GetComponent<Views.MultiGameEndUIView>();
			
            if(view == null)
            {
                Debug.LogError("MultiGameEndUIController could not find associated view component: MultiGameEndUIView");
            }
            view.SetCallback(Exit);
        }

        protected override void OnModelChanged()
        {
        }

        public void StartGameOverSequence(System.Action funcCallback)
        {
            m_funcCallback = funcCallback;
            StartCoroutine("DecideWinner");
        }
        
        public IEnumerator DecideWinner()
        {
            int iMinScore = Mathf.Min(model.PlayerOneScore, model.PlayerTwoScore);
            view.SetText("And...");
            view.Bangup((int)(iMinScore * 0.2f), (int)(iMinScore * 0.3f), 1.5f);
            Messages.PlayerTurnMessage.Send(new Messages.PlayerTurnMessage(Types.PlayerID.One));
            yield return new WaitForSeconds(1.5f);
            view.SetText("And... The Winner... ");
            view.Bangup((int)(iMinScore * 0.6f), (int)(iMinScore * 0.5f), 1.5f);
            Messages.PlayerTurnMessage.Send(new Messages.PlayerTurnMessage(Types.PlayerID.Two));
            yield return new WaitForSeconds(1.5f);
            view.SetText("And... The Winner... Is...");
            view.Bangup(iMinScore, iMinScore, 1.5f);
            Messages.PlayerTurnMessage.Send(new Messages.PlayerTurnMessage(Types.PlayerID.None));
            yield return new WaitForSeconds(1.5f);
            SetPlayerScores();
            view.ShowGameOverButtons();
            yield return null;
        }

        public void SetPlayerScores()
        {
            view.SetsScoreTexts(model.PlayerOneScore.ToString(), model.PlayerTwoScore.ToString());

            if (model.PlayerOneScore > model.PlayerTwoScore)
            {
                view.SetText("Blue Player Wins!");
                Messages.PlayerTurnMessage.Send(new Messages.PlayerTurnMessage(Types.PlayerID.One));
            }
            else if(model.PlayerTwoScore > model.PlayerOneScore)
            {
                view.SetText("Red Player Wins!");
                Messages.PlayerTurnMessage.Send(new Messages.PlayerTurnMessage(Types.PlayerID.Two));
            }
            else
            {
                view.SetText("It's a Tie!");
            }
        }

        public void Exit()
        {
            m_funcCallback();
        }

    }
}
