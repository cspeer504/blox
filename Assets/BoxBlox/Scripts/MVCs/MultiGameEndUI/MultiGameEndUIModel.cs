using UnityEngine;
using System.Collections;

namespace Blox.Models
{
    public class MultiGameEndUIModel : CodeControl.Model
    {
        int iPlayerOneScore;
        public int PlayerOneScore
        {
            get { return iPlayerOneScore; }
            set { iPlayerOneScore = value; }
        }

        int iPlayerTwoScore;
        public int PlayerTwoScore
        {
            get { return iPlayerTwoScore; }
            set { iPlayerTwoScore = value; }
        }
    }
}
