using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Blox.Views
{
    public class MultiGameEndUIView : MonoBehaviour
    {
        public UnityEngine.UI.Text MessageText1;
        public UnityEngine.UI.Text MessageText2;
        public UnityEngine.UI.Text ScoreText1;
        public UnityEngine.UI.Text ScoreText2;
        public UnityEngine.UI.Button readyButton1;
        public UnityEngine.UI.Button readyButton2;
        public SpriteRenderer blackoutSprite;

        private System.Action m_funcCallback;
        private float progress;

        private string m_strPlayerOneScoreTween = "ScoreRampUpPlayer1";
        private string m_strPlayerTwoScoreTween = "ScoreRampUpPlayer2";
        private int m_iPlayerOneDisplayScore = 0;
        private int m_iPlayerTwoDisplayScore = 0;

        void Start()
        {
            readyButton1.gameObject.SetActive(false);
            readyButton2.gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
        }

        public void Bangup(int iPlayer1Score, int iPlayer2Score, float fTime)
        {
            if(iPlayer1Score > iPlayer2Score)
            {
                BangupScore(Types.PlayerID.One, iPlayer1Score, fTime, iTween.EaseType.easeInCubic);
                BangupScore(Types.PlayerID.Two, iPlayer2Score, fTime, iTween.EaseType.linear);
            }
            else if(iPlayer1Score > iPlayer2Score)
            {
                BangupScore(Types.PlayerID.One, iPlayer1Score, fTime, iTween.EaseType.linear);
                BangupScore(Types.PlayerID.Two, iPlayer2Score, fTime, iTween.EaseType.easeInCubic);
            }
            else
            {
                BangupScore(Types.PlayerID.One, iPlayer1Score, fTime, iTween.EaseType.linear);
                BangupScore(Types.PlayerID.Two, iPlayer2Score, fTime, iTween.EaseType.linear);
            }
        }
                
        public void BangupScore(Types.PlayerID ePlayerID, int iToScore, float fTime, iTween.EaseType eEaseType)
        {
            int iFromScore = (ePlayerID == Types.PlayerID.One) ? m_iPlayerOneDisplayScore : m_iPlayerTwoDisplayScore;
            if (iFromScore != iToScore)
            {
                string strUpdateCallback = (ePlayerID == Types.PlayerID.One) ? "OnPlayerOneScoreUpdated" : "OnPlayerTwoScoreUpdated";
                iTween.StopByName(strUpdateCallback);

                Hashtable tweenParams = new Hashtable();
                tweenParams.Add("name", (ePlayerID == Types.PlayerID.One) ? m_strPlayerOneScoreTween : m_strPlayerTwoScoreTween);
                tweenParams.Add("from", iFromScore);
                tweenParams.Add("to", iToScore);
                tweenParams.Add("time", fTime);
                tweenParams.Add("easetype", eEaseType);
                tweenParams.Add("onupdate", strUpdateCallback);

                iTween.ValueTo(gameObject, tweenParams);
            }
        }        

        public void OnPlayerOneScoreUpdated(int iScore)
        {
            m_iPlayerOneDisplayScore = iScore;
            ScoreText1.text = iScore.ToString();
        }

        public void OnPlayerTwoScoreUpdated(int iScore)
        {
            m_iPlayerTwoDisplayScore = iScore;
            ScoreText2.text = iScore.ToString();
        }


        public void SetCallback(System.Action funcCallback)
        {
            m_funcCallback = funcCallback;
        }

        public void ShowGameOverButtons()
        {
            readyButton1.gameObject.SetActive(true);
            readyButton2.gameObject.SetActive(true);
            HelperSetButtonTrigger(readyButton1, UnityEngine.EventSystems.EventTriggerType.PointerClick, m_funcCallback);
            HelperSetButtonTrigger(readyButton2, UnityEngine.EventSystems.EventTriggerType.PointerClick, m_funcCallback);
        }

        public void SetsScoreTexts(string strPlayer1, string strPlayer2)
        {
            iTween.StopByName(m_strPlayerOneScoreTween);
            iTween.StopByName(m_strPlayerTwoScoreTween);
            ScoreText1.text = strPlayer1;
            ScoreText2.text = strPlayer2;
        }

        public void SetText(string strText)
        {
            MessageText1.text = strText;
            MessageText2.text = strText;
        }

        private void HelperSetButtonTrigger(UnityEngine.UI.Button objButton, UnityEngine.EventSystems.EventTriggerType eTriggerType, System.Action funcCallback)
        {
            UnityEngine.EventSystems.EventTrigger trigger = objButton.GetComponent<UnityEngine.EventSystems.EventTrigger>();

            if (trigger == null)
                trigger = objButton.gameObject.AddComponent<UnityEngine.EventSystems.EventTrigger>();

            UnityEngine.EventSystems.EventTrigger.Entry entry = new UnityEngine.EventSystems.EventTrigger.Entry();
            entry.eventID = eTriggerType;
            entry.callback.AddListener((eventData) => { funcCallback(); });
            trigger.triggers.Clear();
            trigger.triggers.Add(entry);
        }
    }
}
