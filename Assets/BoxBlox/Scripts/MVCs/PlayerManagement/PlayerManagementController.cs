using UnityEngine;
using System.Collections;

namespace Blox.Controllers
{
    public class PlayerManagementController : CodeControl.Controller<Models.PlayerManagementModel>
    {

        private Views.PlayerManagementView view;
        Controllers.PlayerController m_objPlayerOne = null;
        Controllers.PlayerController m_objPlayerTwo = null;
        

        protected override void OnInitialize()
        {
            //** TODO: How to handle this?

            model.PlayerList = new CodeControl.ModelRefs<Models.PlayerModel>();
            Models.PlayerModel objPlayerModel = new Models.PlayerModel();
            objPlayerModel.name = "Player1";
            objPlayerModel.eID = Types.PlayerID.One;
            m_objPlayerOne = RegisterPlayer("Prefabs/Player/Player", objPlayerModel);
            model.PlayerList.Add(objPlayerModel);
            m_objPlayerOne.transform.SetParent(gameObject.transform);

            Models.PlayerModel objPlayerTwoModel = new Models.PlayerModel();
            objPlayerTwoModel.name = "Player2";
            objPlayerTwoModel.eID = Types.PlayerID.Two;
            m_objPlayerTwo = RegisterPlayer("Prefabs/Player/Player", objPlayerTwoModel);
            model.PlayerList.Add(objPlayerTwoModel);
            m_objPlayerTwo.transform.SetParent(gameObject.transform);

            //m_objPlayerTwo = new Controllers.PlayerController();

            view = GetComponent<Views.PlayerManagementView>();
			
            if(view == null)
            {
                Debug.LogError("PlayerManagementController could not find associated view component: PlayerManagementView");
            }
            
            Messages.GetPlayerMessage.Register(OnGetPlayerMessage);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            Messages.GetPlayerMessage.Unregister(OnGetPlayerMessage);
        }

        public void OnGetPlayerMessage(Messages.GetPlayerMessage msg)
        {
            //** Todo, refactor. 
            switch(msg.ePlayerID)
            {
                case Types.PlayerID.One:
                    msg.fSetPlayerCallback(m_objPlayerOne);
                    break;

                case Types.PlayerID.Two:
                    msg.fSetPlayerCallback(m_objPlayerTwo);
                    break;

                default:
                    Debug.LogError("Player manager does not have player with ID (" + msg.ePlayerID + ") registered!");
                    break;
            }
        }

        public Controllers.PlayerController GetPlayer(int iIndex)
        {
            return (iIndex == 0) ? m_objPlayerOne : m_objPlayerTwo;
        }

        private Controllers.PlayerController RegisterPlayer(string strResourceLocation, Models.PlayerModel objModel)
        {
            return CodeControl.Controller.Instantiate<Controllers.PlayerController>(Resources.Load(strResourceLocation), objModel);
        }



        protected override void OnModelChanged()
        {
        }
    }
}
