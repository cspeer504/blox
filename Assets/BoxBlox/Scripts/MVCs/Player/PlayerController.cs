﻿using UnityEngine;
using System.Collections;

namespace Blox.Controllers
{
    public class PlayerController : CodeControl.Controller<Models.PlayerModel>
    {

        private Views.PlayerView view;
        private ArrayList m_aPowerUpTemplates = new ArrayList();

        protected override void OnInitialize()
        {
            m_aPowerUpTemplates.Add(createPowerUp(Types.PowerUp.Type.Randomizer));
            m_aPowerUpTemplates.Add(createPowerUp(Types.PowerUp.Type.Colorblind));
            m_aPowerUpTemplates.Add(createPowerUp(Types.PowerUp.Type.TimeBoost));
            m_aPowerUpTemplates.Add(createPowerUp(Types.PowerUp.Type.ColorBoost));
            m_aPowerUpTemplates.Add(createPowerUp(Types.PowerUp.Type.QuantityBoost));
            
            view = GetComponent<Views.PlayerView>();
            if(view == null)
            {
                Debug.LogError("PlayerController could not find associated view component: PlayerView");
            }
        }

        private Types.PowerUp createPowerUp(Types.PowerUp.Type eID)
        {
            Types.PowerUp objPower = new Types.PowerUp();
            objPower.eID = eID;
            switch (eID)
            {
                case Types.PowerUp.Type.None:
                    break;

                case Types.PowerUp.Type.Randomizer:
                    objPower.strName = "Randomizer";
                    objPower.strDescription = "Changes every cell on the board to a random color for the next round.";
                    objPower.strTips = "Use this power up when the board isn't working in your favor.";
                    break;

                case Types.PowerUp.Type.Colorblind:
                    objPower.strName = "Colorblind";
                    objPower.strDescription = "Color is ignored for the next round, every block around the one you touch matches.";
                    objPower.strTips = "Use this power if there aren't any big matches available.";
                    break;

                case Types.PowerUp.Type.TimeBoost:
                    objPower.strName = "Time Boost";
                    objPower.strDescription = "You get 20% more time the next round.";
                    objPower.strTips = "Use this power up to give you more time to out score your opponent!";
                    break;

                case Types.PowerUp.Type.ColorBoost:
                    objPower.strName = "Color Boost";
                    objPower.strDescription = "A Random color gets a 25% score increase for the next round.";
                    objPower.strTips = "This could be the best power up, or the worst. Are you willing to gamble?";
                    break;

                case Types.PowerUp.Type.QuantityBoost:
                    objPower.strName = "Quantity Boost";
                    objPower.strDescription = "You get an extra point for each block in a match for the next round.";
                    objPower.strTips = "This power up is less risky than ColorBoost and more stable. But that means potentially less rewards.";
                    break;

                default:
                    Debug.LogWarning("Can not find power up of type " + eID.ToString());
                    break;

            }
            return objPower;
        }

        public Types.PlayerID GetPlayerID()
        {
            return model.eID;
        }
        
        public ArrayList getPowers()
        {
            return model.aPowerUps;
        }

        public uint hasHowManyPowers()
        {
            return (uint) model.aPowerUps.Count;
        }

        public uint hasPower(Types.PowerUp.Type ePower)
        {
            uint uiCount = 0;

            foreach (Types.PowerUp objPowerUp in model.aPowerUps)
            {
                if (objPowerUp.eID == ePower)
                {
                    uiCount++;
                }
            }

            return uiCount;
        }

        public bool addPower(Types.PowerUp.Type ePower)
        {
            bool bSuccess = false;

            foreach (Types.PowerUp objPowerUp in m_aPowerUpTemplates)
            {
                if (objPowerUp.eID == ePower)
                {
                    model.aPowerUps.Add(createPowerUp(ePower));
                    bSuccess = true;
                    break;
                }
            }

            if (bSuccess == false)
                Debug.LogWarning("Could not find a power up template for type " + ePower.ToString());

            return bSuccess;
        }

        public void removeAllPowers()
        {
            model.aPowerUps.Clear();
        }

        public bool removePower(Types.PowerUp.Type ePower)
        {
            return removePower(ePower, 1);
        }

        public bool removePower(Types.PowerUp.Type ePower, uint uiCount)
        {
            bool bFound = false;
            ArrayList aPowersFound = new ArrayList();

            foreach (Types.PowerUp objPowerUp in model.aPowerUps)
            {
                if (aPowersFound.Count >= uiCount)
                    break;

                if (objPowerUp.eID == ePower)
                {
                    bFound = true;
                    aPowersFound.Add(objPowerUp);
                }
            }

            foreach (Types.PowerUp objPowerUp in aPowersFound)
            {
                model.aPowerUps.Remove(objPowerUp);
            }

            return bFound;
        }

        public Types.PowerUp getPowerUpTemplate(Types.PowerUp.Type eID)
        {
            Types.PowerUp objPowerUpTemplate = new Types.PowerUp();

            foreach (Types.PowerUp objPowerUp in m_aPowerUpTemplates)
            {
                if(objPowerUp.eID == eID)
                {
                    objPowerUpTemplate = createPowerUp(eID);
                    break;
                }
            }
            return objPowerUpTemplate;
        }

        public ArrayList getPowerUpTemplates()
        {
            //** Is there a better way to copy array list?
            ArrayList aPowerUpTemplates = new ArrayList();

            foreach (Types.PowerUp objPowerUp in m_aPowerUpTemplates)
            {
                aPowerUpTemplates.Add(createPowerUp(objPowerUp.eID));
            }
            return aPowerUpTemplates;
        }

        public void ClearActivePowers()
        {
            Messages.BoardActionMessage.Send(new Messages.BoardActionMessage(Types.BoardAction.Clear));
        }

        public bool usePowerUp(Types.PowerUp.Type eID)
        {
            if (hasPower(eID) <= 0)
                return false;

            removePower(eID);

            switch (eID)
            { 
                case Types.PowerUp.Type.None:
                    Debug.LogError("PowerUpIDs.None not implemented");
                    break;

                case Types.PowerUp.Type.Randomizer:
                    Messages.BoardActionMessage.Send(new Messages.BoardActionMessage(Types.BoardAction.Randomize));
                    break;

                case Types.PowerUp.Type.Colorblind:
                    Messages.BoardActionMessage.Send(new Messages.BoardActionMessage(Types.BoardAction.Colorblind));
                    break;

                case Types.PowerUp.Type.TimeBoost:
                    Messages.AddRoundTimeMessage.Send(new Messages.AddRoundTimeMessage(0, 0.25f));
                    break;

                case Types.PowerUp.Type.ColorBoost:
                    Messages.BoardActionMessage.Send(new Messages.BoardActionMessage(Types.BoardAction.ColorBoost));
                    break;

                case Types.PowerUp.Type.QuantityBoost:
                    Messages.BoardActionMessage.Send(new Messages.BoardActionMessage(Types.BoardAction.QuantityBoost));
                    break;

                default:
                    Debug.LogWarning("Power up does not exist. Type " + eID.ToString());
                    break;
            }

            return true;
        }
    }
}
