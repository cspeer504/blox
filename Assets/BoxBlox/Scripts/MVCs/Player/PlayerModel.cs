﻿using UnityEngine;
using System.Collections;

namespace Blox.Models
{
    public class PlayerModel : CodeControl.Model
    {

        public string name;
        public ArrayList aPowerUps = new ArrayList();
        public Types.PlayerID eID;
    }
}
