using UnityEngine;
using System.Collections;

namespace Blox.Controllers
{
    public class BGHighlightsController : CodeControl.Controller<Models.BGHighlightsModel>
    {

        public Types.PlayerID playerID;
        public bool bMove = false;
        private bool bIsMoving = false;

        private Views.BGHighlightsView view;

        /*  // To Instantiate me, use one of the following:
            Models.BGHighlightsModel objBGHighlightsModel = new Models.BGHighlightsModel();
			Controllers.BGHighlightsController m_objBGHighlightsController = Controller.Instantiate<Controllers.BGHighlightsController>(objBGHighlightsModel); //** Create on a new (Empty) Game Object
            Controllers.BGHighlightsController m_objBGHighlightsController = Controller.Instantiate<Controllers.BGHighlightsController>(Resources.Load("PathToPrefabInAnyResourceFolder"), objBGHighlightsModel); //** TODO: Change to correct resource folder.
		*/

        protected override void OnInitialize()
        {
            view = GetComponent<Views.BGHighlightsView>();
			
            if(view == null)
            {
                Debug.LogError("BGHighlightsController could not find associated view component: BGHighlightsView");
            }
            Messages.PlayerTurnMessage.Register(OnPlayerTurnMessage);

            //StartCoroutine("MyUpdate");
        }

        protected override void OnDestroy()
        {
            Messages.PlayerTurnMessage.Unregister(OnPlayerTurnMessage);

            base.OnDestroy();
        }

        public void OnPlayerTurnMessage(Messages.PlayerTurnMessage msg)
        {
            if(msg.PlayerType == playerID)
            {
                view.Show();
            }
            else
            {
                view.Hide();
            }
        }

        protected override void OnModelChanged()
        {
        }

        private IEnumerator MyUpdate()
        {
            while (true)
            {
                if(bIsMoving == false)
                {
                    if(bMove)
                    {
                        bIsMoving = true;
                        view.Show();
                    }
                }
                else
                {
                    if (bMove == false)
                    {
                        bIsMoving = false;
                        view.Hide();
                    }
                }
                yield return null;
            }
        }
    }
}
