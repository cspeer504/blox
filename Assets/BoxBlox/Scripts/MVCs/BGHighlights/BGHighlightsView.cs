using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Blox.Views
{
    public class BGHighlightsView : MonoBehaviour
    {
        public Vector3 offPosition;
        public Vector3 onPosition;
        public float fTimeToTravel = 2f;
        private string strShowTweenName = "nothing";
        private string strHideTweenName = "nothing";

        void Awake()
        {
            gameObject.transform.position = offPosition;
            strShowTweenName = "BGHighlightsView" + gameObject.name + "Show";
            strHideTweenName = "BGHighlightsView" + gameObject.name + "Hide";
            
        }
        
        void OnDestroy()
        {
            iTween.StopByName(strHideTweenName);
            iTween.StopByName(strShowTweenName);
        }

        // Update is called once per frame
        void Update()
        {
        }

        public void Show()
        {
            iTween.StopByName(strHideTweenName);
            iTween.MoveTo(gameObject, iTween.Hash("name", strShowTweenName, "position", onPosition, "time", fTimeToTravel, "oncompletetarget", gameObject, "oncomplete", "OnShowComplete"));
        }

        public void OnShowComplete()
        {

        }

        public void Hide()
        {
            iTween.StopByName(strShowTweenName);
            iTween.MoveTo(gameObject, iTween.Hash("name", strHideTweenName, "position", offPosition, "time", fTimeToTravel, "oncompletetarget", gameObject, "oncomplete", "OnHideComplete"));
        }

        public void OnHideComplete()
        {

        }
    }
}
