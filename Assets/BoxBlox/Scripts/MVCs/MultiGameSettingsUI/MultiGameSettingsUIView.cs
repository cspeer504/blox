using UnityEngine;
using UnityEngine.UI;

namespace Blox.Views
{
    public class MultiGameSettingsUIView : MonoBehaviour
    {
        public uint[]   roundNumbers;
        public Button[] roundButtons;
        public uint[]   turnNumbers;
        public Button[] turnButtons;
        public uint[]   powerUpNumbers;
        public Button[] powerUpButtons;
        public AudioSource powerButtonAudio;
        public Button readyButton;
        public AudioSource readyButtonAudio;

        private uint m_uiRoundNumber = 0;
        private uint m_uiTurnTime = 0;
        private uint m_uiPowerUpCount = 0;

        void Start()
        {
            if (roundButtons.Length > roundNumbers.Length)
                Debug.LogError("roundButtons.Length (" + roundButtons.Length + ") must be equal or less than roundNumbers.Length (" + roundNumbers.Length + ")");

            if (turnButtons.Length > turnNumbers.Length)
                Debug.LogError("turnButtons.Length (" + turnButtons.Length + ") must be equal or less than turnNumbers.Length (" + turnNumbers.Length + ")");

            if (powerUpButtons.Length > powerUpNumbers.Length)
                Debug.LogError("powerUpButtons.Length (" + powerUpButtons.Length + ") must be equal or less than powerUpNumbers.Length (" + powerUpNumbers.Length + ")");

            for (int iIndex = 0; iIndex < roundButtons.Length; ++iIndex)
            {
                HelperSetButtonText(roundButtons[iIndex], roundNumbers[iIndex].ToString());
                HelperSetButtonTrigger(roundButtons[iIndex], roundNumbers[iIndex], UnityEngine.EventSystems.EventTriggerType.PointerClick, RoundNumberButtonClicked);
            }
            m_uiRoundNumber = roundNumbers[0];
            HelperDisableAllButtonsExcept(roundButtons, roundNumbers, roundNumbers[0]);

            for (int iIndex = 0; iIndex < turnButtons.Length; ++iIndex)
            {
                HelperSetButtonText(turnButtons[iIndex], turnNumbers[iIndex].ToString());
                HelperSetButtonTrigger(turnButtons[iIndex], turnNumbers[iIndex], UnityEngine.EventSystems.EventTriggerType.PointerClick, TurnNumberButtonClicked);
            }
            m_uiTurnTime = turnNumbers[0];
            HelperDisableAllButtonsExcept(turnButtons, turnNumbers, turnNumbers[0]);

            for (int iIndex = 0; iIndex < powerUpButtons.Length; ++iIndex)
            {
                HelperSetButtonText(powerUpButtons[iIndex], powerUpNumbers[iIndex].ToString());
                HelperSetButtonTrigger(powerUpButtons[iIndex], powerUpNumbers[iIndex], UnityEngine.EventSystems.EventTriggerType.PointerClick, PowerUpNumberButtonClicked);
            }
            m_uiPowerUpCount = powerUpNumbers[0];
            HelperDisableAllButtonsExcept(powerUpButtons, powerUpNumbers, powerUpNumbers[0]);

            HelperSetButtonTrigger(readyButton, 0, UnityEngine.EventSystems.EventTriggerType.PointerClick, ReadyButtonClicked);
        }

        public void RoundNumberButtonClicked(uint uiValue)
        {
            m_uiRoundNumber = uiValue;
            HelperDisableAllButtonsExcept(roundButtons, roundNumbers, uiValue);
            powerButtonAudio.Play();
        }

        public void TurnNumberButtonClicked(uint uiValue)
        {
            m_uiTurnTime = uiValue;
            HelperDisableAllButtonsExcept(turnButtons, turnNumbers, uiValue);
            powerButtonAudio.Play();
        }

        public void PowerUpNumberButtonClicked(uint uiValue)
        {
            m_uiPowerUpCount = uiValue;
            HelperDisableAllButtonsExcept(powerUpButtons, powerUpNumbers, uiValue);
            powerButtonAudio.Play();
        }

        public void ReadyButtonClicked(uint uiValue)
        {
            readyButtonAudio.Play();
            Messages.MultiplayerGameSettingsMessage.Send(new Messages.MultiplayerGameSettingsMessage(m_uiRoundNumber, m_uiTurnTime, m_uiPowerUpCount));
        }

        // Update is called once per frame
        void Update()
        {
        }
        
        private void HelperSetButtonTrigger(UnityEngine.UI.Button objButton, uint uiCount, UnityEngine.EventSystems.EventTriggerType eTriggerType, System.Action<uint> funcCallback)
        {
            UnityEngine.EventSystems.EventTrigger trigger = objButton.GetComponent<UnityEngine.EventSystems.EventTrigger>();

            if (trigger == null)
                trigger = objButton.gameObject.AddComponent<UnityEngine.EventSystems.EventTrigger>();

            UnityEngine.EventSystems.EventTrigger.Entry entry = new UnityEngine.EventSystems.EventTrigger.Entry();
            entry.eventID = eTriggerType;
            entry.callback.AddListener((eventData) => { funcCallback(uiCount); });
            trigger.triggers.Clear();
            trigger.triggers.Add(entry);
        }

        private void HelperDisableAllButtonsExcept(UnityEngine.UI.Button[] objButtons, uint[] uiValues, uint uiValueToShow)
        {
            for (int iIndex = 0; iIndex < objButtons.Length; ++iIndex)
            {
                objButtons[iIndex].GetComponentInChildren<UnityEngine.UI.Text>().color = (uiValues[iIndex] == uiValueToShow) ? Color.green : Color.grey;
            }
        }

        private void HelperSetButtonText(UnityEngine.UI.Button objButton, string strText)
        {
            UnityEngine.UI.Text objText = objButton.gameObject.GetComponentInChildren<UnityEngine.UI.Text>();

            if (objText != null)
            {
                objText.text = strText;
            }
        }
    }
}
