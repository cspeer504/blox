using UnityEngine;
using System.Collections;

namespace Blox.Controllers
{
    public class MultiGameSettingsUIController : CodeControl.Controller<Models.MultiGameSettingsUIModel>
    {

        private Views.MultiGameSettingsUIView view;
				
		/*  // To Instantiate me, use one of the following:
            Models.MultiGameSettingsUIModel objMultiGameSettingsUIModel = new Models.MultiGameSettingsUIModel();
			Controllers.MultiGameSettingsUIController m_objMultiGameSettingsUIController = Controller.Instantiate<Controllers.MultiGameSettingsUIController>(objMultiGameSettingsUIModel); //** Create on a new (Empty) Game Object
            Controllers.MultiGameSettingsUIController m_objMultiGameSettingsUIController = Controller.Instantiate<Controllers.MultiGameSettingsUIController>(Resources.Load("PathToPrefabInAnyResourceFolder"), objMultiGameSettingsUIModel); //** TODO: Change to correct resource folder.
		*/

        protected override void OnInitialize()
        {
            view = GetComponent<Views.MultiGameSettingsUIView>();
			
            if(view == null)
            {
                Debug.LogError("MultiGameSettingsUIController could not find associated view component: MultiGameSettingsUIView");
            }
        }

        protected override void OnModelChanged()
        {
        }
    }
}
