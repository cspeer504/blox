using UnityEngine;
using System.Collections;

namespace Blox.Controllers
{
    public class MultiplayerHudController : CodeControl.Controller<Models.MultiplayerHudModel>
    {
        private Views.MultiplayerHudView view;
        
        private bool m_bLastStatus = false;
        private float m_fRoundTimer = 0f;
        private float m_fMenuTimer = 0f;

        private System.Action m_funcOnTimerCompleteCallback;
        private System.Action m_funcPowerSelectedCallback;
        private Controllers.PlayerController m_objPlayer;
        
        //public delegate void PowerUpSelectedDelegate(Types.PowerUp.Type ePowerUp);
        
        protected override void OnInitialize()
        {
            view = GetComponent<Views.MultiplayerHudView>();
            if (view == null)
            {
                Debug.LogError("PlayerController could not find associated view component: PlayerView");
            }
            m_bLastStatus = model.active;
            Messages.GetPlayerMessage.Send(new Messages.GetPlayerMessage(model.PlayerID, OnGetPlayer));

            view.SetRoundsLeft(model.roundsLeft);
            //view.SendPowerUpSelectedCallback += SendPowerUpSelectedCallback;
        }
        
        protected override void OnDestroy()
        {
            Messages.MatchMessage.Unregister(OnMatchMessage);
            Messages.AddRoundTimeMessage.Unregister(OnAddTimeMessage);
            m_objPlayer.removeAllPowers();
            base.OnDestroy();
        }

        public void OnModelActivate(bool bActive)
        {
            if (bActive)
            {
                Messages.MatchMessage.Register(OnMatchMessage);
                Messages.AddRoundTimeMessage.Register(OnAddTimeMessage);
            }
            else
            {
                Messages.MatchMessage.Unregister(OnMatchMessage);
                Messages.AddRoundTimeMessage.Unregister(OnAddTimeMessage);
                //StartCoroutine("InactiveMessage");
            }
        }

        public void SetActive(bool bFlag)
        {
            model.active = bFlag;
            model.NotifyChange();
        }

        public void OnGetPlayer(Controllers.PlayerController objPlayer)
        {
            m_objPlayer = objPlayer;      
        }

        public void SendPowerUpSelectedCallback(Types.PowerUp.Type ePowerUp)
        {
            view.EnablePowerHUD(false, null);
            
            //** Must call this before using power. So HUD will be listening. 
            m_funcPowerSelectedCallback();

            //** TODO Send a message "Use Power Up". Send name (ID) and ePowerType.
            if (ePowerUp != Types.PowerUp.Type.None)
                m_objPlayer.usePowerUp(ePowerUp);
        }

        public void EnablePowerHUD(bool bFlag, System.Action funcCallback)
        {
            m_funcPowerSelectedCallback = funcCallback;
            //PowerUpSelectedDelegate myDelegate = new PowerUpSelectedDelegate(this.SendPowerUpSelectedCallback);
            view.EnablePowerHUD(bFlag, SendPowerUpSelectedCallback);

            if(bFlag)
            {
                view.SetMenuTimer(Mathf.CeilToInt(model.menuTimeInSeconds));
                view.SetPowers(m_objPlayer.getPowers());
                StartCoroutine("RunPowerUpSelectionTimer");
            }
        }

        public void PerpareForNextTurn()
        {
            view.SetUserMessage("Get Ready!");
            view.SetTimePercentage(1);
        }

        public void OtherPlayerIsActive()
        {
            view.SetUserMessage("Please Wait...");
        }
        
        public void StartTurnTimer(System.Action funcCallback)
        {
            m_funcOnTimerCompleteCallback = funcCallback;
            StartCoroutine("RunGameTimer");
        }

        private IEnumerator InactiveMessage()
        {
            yield return new WaitForSeconds(1f);
            view.SetUserMessage("Please Wait...");
        }

        private IEnumerator RunGameTimer()
        {
            if (model.roundsLeft == 1)
            {
                StartCoroutine("RandomizeScore");
            }

            float countdownTime = 3f;
            float startTime = Time.time;
            m_fRoundTimer += model.turnTimeInSeconds;
            while (Time.time - startTime < m_fRoundTimer)
            {
                float fTime = Time.time - startTime;
                //float progress = (Time.time - startTime) / runTime;
                float fTimeLeft = m_fRoundTimer - fTime;
                fTimeLeft = Mathf.Max(fTimeLeft, 0.0f);
                view.SetUserMessage(fTimeLeft.ToString("0.00") + " seconds left");
                view.SetTimePercentage(fTimeLeft / m_fRoundTimer);
                
                if(fTimeLeft < countdownTime)
                {
                    view.Countdown(fTimeLeft);
                    countdownTime -= 1f;
                }
                yield return null;
            }
            view.TurnOver();
            model.roundsLeft = (model.roundsLeft != 0) ? model.roundsLeft - 1 : 0;
            model.NotifyChange();
            
            m_fRoundTimer = 0f;
            m_objPlayer.ClearActivePowers();
            view.SetUserMessage("Times Up!");
            view.SetTimePercentage(0);
            m_funcOnTimerCompleteCallback();
        }

        private IEnumerator RandomizeScore()
        {
            while (model.roundsLeft <= 1)
            {
                string strRandom = Random.Range(0, 2).ToString();
                strRandom += Random.Range(0, 2).ToString();
                strRandom += Random.Range(0, 2).ToString();
                strRandom += Random.Range(0, 2).ToString();
                view.SetPlayerScoreText(strRandom);
                yield return new WaitForSeconds(0.1f);
            }
        }

        private IEnumerator RunPowerUpSelectionTimer()
        {
            float startTime = Time.time;
            m_fMenuTimer = model.menuTimeInSeconds;
            while (Time.time - startTime < m_fMenuTimer)
            {
                //float progress = (Time.time - startTime) / runTime;
                float fTimeLeft = m_fMenuTimer - (Time.time - startTime);
                fTimeLeft = Mathf.Max(fTimeLeft, 0.0f);
                view.SetMenuTimer(Mathf.CeilToInt(fTimeLeft));
                yield return null;
            }

            view.OnPowerButtonPress(Types.PowerUp.Type.None);
        }


        protected override void OnModelChanged()
        {
            Messages.PlayerScoreMessage.Send(new Messages.PlayerScoreMessage(model.PlayerID, model.score));

            if(model.roundsLeft > 1)
            {
                view.SetPlayerScoreText(model.score.ToString());
            }

            view.SetRoundsLeft(model.roundsLeft);

            if (model.active != m_bLastStatus)
            {
                OnModelActivate(model.active);
                m_bLastStatus = model.active;
            }
        }

        private void OnMatchMessage(Messages.MatchMessage m)
        {
            if(m.count <= 0)
            {
                return;
            }

            int iPointsEarned = m.count * m.count;

            bool bColorBoostMatch = m.type == Types.MatchType.ColorBoostRed25 && m.color == Types.TileColor.Red;
            bColorBoostMatch |= m.type == Types.MatchType.ColorBoostBlue25 && m.color == Types.TileColor.Blue;
            bColorBoostMatch |= m.type == Types.MatchType.ColorBoostGreen25 && m.color == Types.TileColor.Green;
            bColorBoostMatch |= m.type == Types.MatchType.ColorBoostYellow25 && m.color == Types.TileColor.Yellow;


            if (bColorBoostMatch)
            {
                int iBoost = (int)(iPointsEarned * 0.25);
                iPointsEarned += iBoost;
               // scoreModel.strExtraInfo = "+" + iBoost + " Color boost!";
                //scoreModel.strExtraInfo += " (+ " + iBoost + ")";
            }
            else if(m.type == Types.MatchType.QuantityBoost1)
            {
                iPointsEarned += m.count;
                //scoreModel.strExtraInfo = "+" + m.count + " Quantity boost!";
                //scoreModel.strExtraInfo += " (+ " + m.count + ")";
            }

            model.score += iPointsEarned;
            model.NotifyChange();

            Models.MatchScoreModel scoreModel = new Models.MatchScoreModel();
            scoreModel.score = iPointsEarned;
            scoreModel.strExtraInfo = "+ " + iPointsEarned;
            Controllers.MatchScoreController objMathScore = null;

            if (m_objPlayer.GetPlayerID() == Types.PlayerID.One)
            {
                objMathScore = CodeControl.Controller.Instantiate<Controllers.MatchScoreController>(Resources.Load("Prefabs/UI/BlueScoreNotification"), scoreModel);
            }
            else if(m_objPlayer.GetPlayerID() == Types.PlayerID.Two)
            {
                objMathScore = CodeControl.Controller.Instantiate<Controllers.MatchScoreController>(Resources.Load("Prefabs/UI/RedScoreNotification"), scoreModel);
            }
            else
            {
                Debug.LogWarning("Can't handle score showing for player " + m_objPlayer.GetPlayerID());
            }

            if(objMathScore != null)
            {
                objMathScore.gameObject.transform.SetParent(gameObject.transform, false);
                objMathScore.ShowScoreText();
            }
            else
            {
                Debug.LogError("Can not find MatchScoreController prefab in the resource folder for player " + m_objPlayer.GetPlayerID());
            }
        }

        private void OnAddTimeMessage(Messages.AddRoundTimeMessage m)
        {
            if(m.percentageGain > 0 || m.percentageGain < 0)
            {
                m_fRoundTimer += m.percentageGain * model.turnTimeInSeconds;
            }
            else
            {
                m_fRoundTimer += m.timeInSeconds;
            }
        }
    }
}
