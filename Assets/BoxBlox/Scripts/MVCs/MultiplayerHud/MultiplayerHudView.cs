using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Blox.Views
{
    public class MultiplayerHudView : MonoBehaviour
    {
        public Text playerScoreText;
        public Text playerMessageText;
        public Text roundsLeftText;
        public Text objMenuItemText;
        public Slider timeLeftSlider;
        public GameObject powerUpHUD;

        public AudioSource countdownBeepAudio;
        public AudioSource turnOverAudio;

        public Button[] aPowerButtons;
        public AudioSource powerButtonAudio;
        public Button objReadyButton;
        public AudioSource readyButtonAudio;
        private Types.PowerUp.Type[] m_aPowerUpsPerButton;

        
        //** Controller callback when a power up is selected        
        private System.Action<Types.PowerUp.Type> m_funcPowerUpSelectedCallback;
        
        void Awake()
        {
            if (playerScoreText == null)
            {
                Debug.Assert(false, "PlayerView needs a score text mesh!");
            }
            
            if (powerUpHUD == null)
            {
                Debug.LogWarning("Could not find the Power Up HUD!");
            }
            HelperSetButtonTrigger(objReadyButton, Types.PowerUp.Type.None, EventTriggerType.PointerClick, OnNoneButtonPress);
        }
        
        void Update() {}

        public void Countdown(float fTime)
        {
            countdownBeepAudio.Play();
        }

        public void TurnOver()
        {
            turnOverAudio.Play();
        }

        public void SetPowers(ArrayList aPowers)
        {
            m_aPowerUpsPerButton = new Types.PowerUp.Type[aPowers.Count];

            if (aPowers.Count > aPowerButtons.Length)
            {
                Debug.LogWarning("Powers Length (" + aPowers.Count + ") is more than buttons length (" + aPowerButtons.Length + ")!");
            }

            for (int iIndex = 0; iIndex < aPowerButtons.Length; ++iIndex)
            {
                aPowerButtons[iIndex].gameObject.SetActive(false);
            }

            for (int iIndex = 0; iIndex < aPowers.Count; ++iIndex)
            {
                Types.PowerUp objPower = (Types.PowerUp) aPowers[iIndex];
                if (iIndex < aPowerButtons.Length)
                {
                    aPowerButtons[iIndex].gameObject.SetActive(true);
                    Text objText = aPowerButtons[iIndex].gameObject.GetComponentInChildren<Text>();
                    
                    m_aPowerUpsPerButton[iIndex] = objPower.eID;

                    if (objText != null)
                    {
                        objText.text = objPower.strName;
                    }
                    else
                    {
                        Debug.LogError("Button '" + aPowerButtons[iIndex].name + "' does not have a 'Text' GO child. Is Button Active?");
                    }
                    HelperSetButtonTrigger(aPowerButtons[iIndex], objPower.eID, EventTriggerType.PointerClick, OnPowerButtonPress);
                }
            }

            if(aPowers.Count == 0)
            {
                OnPowerButtonPress(Types.PowerUp.Type.None);
            }
        }

        public void EnablePowerHUD(bool bFlag, System.Action<Types.PowerUp.Type> funcCallback)
        {
            m_funcPowerUpSelectedCallback = funcCallback;
            powerUpHUD.SetActive(bFlag);
        }

        public void OnPowerButtonPress(Types.PowerUp.Type eType)
        {
            if (m_funcPowerUpSelectedCallback != null)
            {
                powerButtonAudio.Play();
                m_funcPowerUpSelectedCallback(eType);
            }
        }

        public void OnNoneButtonPress(Types.PowerUp.Type eType)
        {
            if (m_funcPowerUpSelectedCallback != null)
            {
                readyButtonAudio.Play();
                m_funcPowerUpSelectedCallback(eType);
            }
        }

        public void SetPlayerScoreText(string strMSG)
        {
            playerScoreText.text = strMSG;
        }

        public void SetRoundsLeft(uint iRoundsLeft)
        {
            roundsLeftText.text = iRoundsLeft.ToString();
        }

        public void SetUserMessage(string strMessage)
        {
            playerMessageText.text = strMessage;
        }

        public void SetTimePercentage(float fTime)
        {
            timeLeftSlider.normalizedValue = fTime;
        }

        public void SetMenuTimer(int iTime)
        {
            objMenuItemText.text = iTime.ToString();
        }

        private void HelperSetButtonTrigger(UnityEngine.UI.Button objButton, Types.PowerUp.Type eType, EventTriggerType eTriggerType, System.Action<Types.PowerUp.Type> funcCallback)
        {
            EventTrigger trigger = objButton.GetComponent<EventTrigger>();

            if(trigger == null)
                trigger = objButton.gameObject.AddComponent<EventTrigger>();

            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = eTriggerType;
            entry.callback.AddListener((eventData) => { funcCallback(eType); });
            trigger.triggers.Clear();
            trigger.triggers.Add(entry);
        }
    }
}
