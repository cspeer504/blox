using UnityEngine;
using System.Collections;

namespace Blox.Models
{
    public class MultiplayerHudModel : CodeControl.Model
    {
        //** TODO: Take name from PlayerModel
        public string name;
        public Types.PlayerID PlayerID;
        public bool active;
        public int score;
        public uint roundsLeft;
        public float turnTimeInSeconds;
        public float menuTimeInSeconds;
    }
}
