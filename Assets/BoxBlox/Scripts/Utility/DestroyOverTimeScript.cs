﻿using UnityEngine;
using System.Collections;

public class DestroyOverTimeScript : MonoBehaviour {

    public float fTimeToDestory = 1f;
    float fStartTime = 0f;
	// Use this for initialization
	void Start ()
    {
        fStartTime = Time.time;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(Time.time - fStartTime > fTimeToDestory)
        {
            GameObject.Destroy(gameObject);
        }
	}
}
