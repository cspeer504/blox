using UnityEngine;
using System.Collections;
using Blox;

namespace Blox.Messages
{
    public class WinnerMessage : CodeControl.Message
    {
        //** Public constructor for easy assembly
        public WinnerMessage(Types.WinnerCallout eCallout)
        {
            Callout = eCallout;
        }

        //** Data
        Types.WinnerCallout eCallout;
        public Types.WinnerCallout Callout
        {
            get { return eCallout; }
            set { eCallout = value; }
        }

        #region Convenience static functions.
        public static void Register(string strMsgName, System.Action<WinnerMessage> callback)
        {
            CodeControl.Message.AddListener<Messages.WinnerMessage>(strMsgName, callback);
        }

        public static void Register(System.Action<WinnerMessage> callback)
        {
            Register("Winner", callback);
        }
        
        public static void Unregister(string strMsgName, System.Action<WinnerMessage> callback)
        {
            CodeControl.Message.RemoveListener<Messages.WinnerMessage>("Winner", callback);
        }

        public static void Unregister(System.Action<WinnerMessage> callback)
        {
            Unregister("Winner", callback);
        }

        public static void Send(string strMsgName, WinnerMessage eMsg)
        {
            CodeControl.Message.Send<Messages.WinnerMessage>(strMsgName, eMsg);
        }

        public static void Send(WinnerMessage eMsg)
        {
            Send("Winner", eMsg);
        }
        #endregion
    }
}