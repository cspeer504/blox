﻿using UnityEngine;
using System.Collections;
using Blox;

namespace Blox.Messages
{
    public class MatchMessage : CodeControl.Message
    {
        //** Public constructor for easy assembly
        public MatchMessage(int iCount, Types.MatchType eType, Blox.Types.TileColor eColor, Vector2 v2Pos)
        {
            count = iCount;
            type = eType;
            color = eColor;
            position = v2Pos;
        }

        //** Data
        public int count = 0;
        public Types.MatchType type = Types.MatchType.Normal;
        public Types.TileColor color = Types.TileColor.Red;
        public Vector2 position = Vector2.zero;

        #region Convenience static functions.
        public static void Register(string strMsgName, System.Action<MatchMessage> callback)
        {
            CodeControl.Message.AddListener<Messages.MatchMessage>(strMsgName, callback);
        }

        public static void Register(System.Action<MatchMessage> callback)
        {
            Register("Match", callback);
        }
        
        public static void Unregister(string strMsgName, System.Action<MatchMessage> callback)
        {
            CodeControl.Message.RemoveListener<Messages.MatchMessage>("Match", callback);
        }

        public static void Unregister(System.Action<MatchMessage> callback)
        {
            Unregister("Match", callback);
        }

        public static void Send(string strMsgName, MatchMessage eMsg)
        {
            CodeControl.Message.Send<Messages.MatchMessage>(strMsgName, eMsg);
        }

        public static void Send(MatchMessage eMsg)
        {
            Send("Match", eMsg);
        }
        #endregion
    }
}