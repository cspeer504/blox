using UnityEngine;
using System.Collections;
using Blox;

namespace Blox.Messages
{
    public class MultiplayerGameSettingsMessage : CodeControl.Message
    {
        //** Public constructor for easy assembly
        public MultiplayerGameSettingsMessage(uint uiRounds, uint uiTurnTime, uint uiPowerCount)
        {
            Rounds = uiRounds;
            TurnTime = uiTurnTime;
            PowerCount = uiPowerCount;
        }

        //** Data
        uint m_uiRounds;
        public uint Rounds
        {
            get { return m_uiRounds; }
            set { m_uiRounds = value; }
        }

        uint m_uiTurnTime;
        public uint TurnTime
        {
            get { return m_uiTurnTime; }
            set { m_uiTurnTime = value; }
        }

        uint m_uiPowerCount;
        public uint PowerCount
        {
            get { return m_uiPowerCount; }
            set { m_uiPowerCount = value; }
        }
        #region Convenience static functions.
        public static void Register(string strMsgName, System.Action<MultiplayerGameSettingsMessage> callback)
        {
            CodeControl.Message.AddListener<Messages.MultiplayerGameSettingsMessage>(strMsgName, callback);
        }

        public static void Register(System.Action<MultiplayerGameSettingsMessage> callback)
        {
            Register("MultiplayerGameSettings", callback);
        }
        
        public static void Unregister(string strMsgName, System.Action<MultiplayerGameSettingsMessage> callback)
        {
            CodeControl.Message.RemoveListener<Messages.MultiplayerGameSettingsMessage>("MultiplayerGameSettings", callback);
        }

        public static void Unregister(System.Action<MultiplayerGameSettingsMessage> callback)
        {
            Unregister("MultiplayerGameSettings", callback);
        }

        public static void Send(string strMsgName, MultiplayerGameSettingsMessage eMsg)
        {
            CodeControl.Message.Send<Messages.MultiplayerGameSettingsMessage>(strMsgName, eMsg);
        }

        public static void Send(MultiplayerGameSettingsMessage eMsg)
        {
            Send("MultiplayerGameSettings", eMsg);
        }
        #endregion
    }
}