using UnityEngine;
using System.Collections;
using Blox;

namespace Blox.Messages
{
    public class CountdownMessage : CodeControl.Message
    {
        //** Public constructor for easy assembly
        public CountdownMessage(Types.CountdownType eType)
        {
            Type = eType;
        }

        //** Data
        Types.CountdownType eType;
        public Types.CountdownType Type
        {
            get { return eType; }
            set { eType = value; }
        }

        #region Convenience static functions.
        public static void Register(string strMsgName, System.Action<CountdownMessage> callback)
        {
            CodeControl.Message.AddListener<Messages.CountdownMessage>(strMsgName, callback);
        }

        public static void Register(System.Action<CountdownMessage> callback)
        {
            Register("Countdown", callback);
        }
        
        public static void Unregister(string strMsgName, System.Action<CountdownMessage> callback)
        {
            CodeControl.Message.RemoveListener<Messages.CountdownMessage>("Countdown", callback);
        }

        public static void Unregister(System.Action<CountdownMessage> callback)
        {
            Unregister("Countdown", callback);
        }

        public static void Send(string strMsgName, CountdownMessage eMsg)
        {
            CodeControl.Message.Send<Messages.CountdownMessage>(strMsgName, eMsg);
        }

        public static void Send(CountdownMessage eMsg)
        {
            Send("Countdown", eMsg);
        }
        #endregion
    }
}