using UnityEngine;
using System.Collections;
using Blox;

namespace Blox.Messages
{
    public class PlayerTurnMessage : CodeControl.Message
    {
        //** Public constructor for easy assembly
        public PlayerTurnMessage(Types.PlayerID ePlayerType)
        {
            PlayerType = ePlayerType;
        }

        //** Data
        Types.PlayerID ePlayerType;
        public Types.PlayerID PlayerType
        {
            get { return ePlayerType; }
            set { ePlayerType = value; }
        }
        #region Convenience static functions.
        public static void Register(string strMsgName, System.Action<PlayerTurnMessage> callback)
        {
            CodeControl.Message.AddListener<Messages.PlayerTurnMessage>(strMsgName, callback);
        }

        public static void Register(System.Action<PlayerTurnMessage> callback)
        {
            Register("PlayerTurn", callback);
        }
        
        public static void Unregister(string strMsgName, System.Action<PlayerTurnMessage> callback)
        {
            CodeControl.Message.RemoveListener<Messages.PlayerTurnMessage>("PlayerTurn", callback);
        }

        public static void Unregister(System.Action<PlayerTurnMessage> callback)
        {
            Unregister("PlayerTurn", callback);
        }

        public static void Send(string strMsgName, PlayerTurnMessage eMsg)
        {
            CodeControl.Message.Send<Messages.PlayerTurnMessage>(strMsgName, eMsg);
        }

        public static void Send(PlayerTurnMessage eMsg)
        {
            Send("PlayerTurn", eMsg);
        }
        #endregion
    }
}