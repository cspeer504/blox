using UnityEngine;
using System.Collections;
using Blox;

namespace Blox.Messages
{
    public class GetPlayerMessage : CodeControl.Message
    {
        //** Public constructor for easy assembly
        public GetPlayerMessage(Types.PlayerID eID, System.Action<Controllers.PlayerController> funcSetPlayerCallback)
        {
            ePlayerID = eID;
            fSetPlayerCallback = funcSetPlayerCallback;
        }

        //** Data
        public Types.PlayerID ePlayerID;
        public System.Action<Controllers.PlayerController> fSetPlayerCallback;

        #region Convenience static functions.
        public static void Register(string strMsgName, System.Action<GetPlayerMessage> callback)
        {
            CodeControl.Message.AddListener<Messages.GetPlayerMessage>(strMsgName, callback);
        }

        public static void Register(System.Action<GetPlayerMessage> callback)
        {
            Register("GetPlayer", callback);
        }
        
        public static void Unregister(string strMsgName, System.Action<GetPlayerMessage> callback)
        {
            CodeControl.Message.RemoveListener<Messages.GetPlayerMessage>("GetPlayer", callback);
        }

        public static void Unregister(System.Action<GetPlayerMessage> callback)
        {
            Unregister("GetPlayer", callback);
        }

        public static void Send(string strMsgName, GetPlayerMessage eMsg)
        {
            CodeControl.Message.Send<Messages.GetPlayerMessage>(strMsgName, eMsg);
        }

        public static void Send(GetPlayerMessage eMsg)
        {
            Send("GetPlayer", eMsg);
        }
        #endregion
    }
}