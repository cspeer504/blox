using UnityEngine;
using System.Collections;
using Blox;

namespace Blox.Messages
{
    public class AddRoundTimeMessage : CodeControl.Message
    {
        //** Public constructor for easy assembly
        public AddRoundTimeMessage(float fTimeInSeconds, float fPercentageGain)
        {
            timeInSeconds = fTimeInSeconds;
            percentageGain = fPercentageGain;
        }

        //** Data
        public float timeInSeconds = 0;
        public float percentageGain = 0;
        #region Convenience static functions.
        public static void Register(string strMsgName, System.Action<AddRoundTimeMessage> callback)
        {
            CodeControl.Message.AddListener<Messages.AddRoundTimeMessage>(strMsgName, callback);
        }

        public static void Register(System.Action<AddRoundTimeMessage> callback)
        {
            Register("AddRoundTime", callback);
        }
        
        public static void Unregister(string strMsgName, System.Action<AddRoundTimeMessage> callback)
        {
            CodeControl.Message.RemoveListener<Messages.AddRoundTimeMessage>("AddRoundTime", callback);
        }

        public static void Unregister(System.Action<AddRoundTimeMessage> callback)
        {
            Unregister("AddRoundTime", callback);
        }

        public static void Send(string strMsgName, AddRoundTimeMessage eMsg)
        {
            CodeControl.Message.Send<Messages.AddRoundTimeMessage>(strMsgName, eMsg);
        }

        public static void Send(AddRoundTimeMessage eMsg)
        {
            Send("AddRoundTime", eMsg);
        }
        #endregion
    }
}