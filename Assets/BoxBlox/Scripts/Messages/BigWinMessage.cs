using UnityEngine;
using System.Collections;
using Blox;

namespace Blox.Messages
{
    public class BigWinMessage : CodeControl.Message
    {
        //** Public constructor for easy assembly
        public BigWinMessage(Types.BigWinType eType)
        {
            Type = eType;
        }

        //** Data
        Types.BigWinType eType;
        public Types.BigWinType Type
        {
            get { return eType; }
            set { eType = value; }
        }

        #region Convenience static functions.
        public static void Register(string strMsgName, System.Action<BigWinMessage> callback)
        {
            CodeControl.Message.AddListener<Messages.BigWinMessage>(strMsgName, callback);
        }

        public static void Register(System.Action<BigWinMessage> callback)
        {
            Register("BigWin", callback);
        }
        
        public static void Unregister(string strMsgName, System.Action<BigWinMessage> callback)
        {
            CodeControl.Message.RemoveListener<Messages.BigWinMessage>("BigWin", callback);
        }

        public static void Unregister(System.Action<BigWinMessage> callback)
        {
            Unregister("BigWin", callback);
        }

        public static void Send(string strMsgName, BigWinMessage eMsg)
        {
            CodeControl.Message.Send<Messages.BigWinMessage>(strMsgName, eMsg);
        }

        public static void Send(BigWinMessage eMsg)
        {
            Send("BigWin", eMsg);
        }
        #endregion
    }
}