using UnityEngine;
using System.Collections;
using Blox;

namespace Blox.Messages
{
    public class LevelLoadedMessage : CodeControl.Message
    {
        //** Public constructor for easy assembly
        public LevelLoadedMessage(Types.LevelID eLevelID)
        {
            levelID = eLevelID;
        }

        //** Data
        public Types.LevelID levelID;

        #region Convenience static functions.
        public static void Register(string strMsgName, System.Action<LevelLoadedMessage> callback)
        {
            CodeControl.Message.AddListener<Messages.LevelLoadedMessage>(strMsgName, callback);
        }

        public static void Register(System.Action<LevelLoadedMessage> callback)
        {
            Register("LevelLoaded", callback);
        }
        
        public static void Unregister(string strMsgName, System.Action<LevelLoadedMessage> callback)
        {
            CodeControl.Message.RemoveListener<Messages.LevelLoadedMessage>("LevelLoaded", callback);
        }

        public static void Unregister(System.Action<LevelLoadedMessage> callback)
        {
            Unregister("LevelLoaded", callback);
        }

        public static void Send(string strMsgName, LevelLoadedMessage eMsg)
        {
            CodeControl.Message.Send<Messages.LevelLoadedMessage>(strMsgName, eMsg);
        }

        public static void Send(LevelLoadedMessage eMsg)
        {
            Send("LevelLoaded", eMsg);
        }
        #endregion
    }
}