using UnityEngine;
using System.Collections;
using Blox;

namespace Blox.Messages
{
    public class MilestoneMessage : CodeControl.Message
    {
        //** Public constructor for easy assembly
        public MilestoneMessage(Types.MilestoneID milestone)
        {
            Milestone = milestone;
        }

        //** Data
        Types.MilestoneID milestone;
        public Types.MilestoneID Milestone
        {
            get { return milestone; }
            set { milestone = value; }
        }
        #region Convenience static functions.
        public static void Register(string strMsgName, System.Action<MilestoneMessage> callback)
        {
            CodeControl.Message.AddListener<Messages.MilestoneMessage>(strMsgName, callback);
        }

        public static void Register(System.Action<MilestoneMessage> callback)
        {
            Register("Milestone", callback);
        }
        
        public static void Unregister(string strMsgName, System.Action<MilestoneMessage> callback)
        {
            CodeControl.Message.RemoveListener<Messages.MilestoneMessage>("Milestone", callback);
        }

        public static void Unregister(System.Action<MilestoneMessage> callback)
        {
            Unregister("Milestone", callback);
        }

        public static void Send(string strMsgName, MilestoneMessage eMsg)
        {
            CodeControl.Message.Send<Messages.MilestoneMessage>(strMsgName, eMsg);
        }

        public static void Send(MilestoneMessage eMsg)
        {
            Send("Milestone", eMsg);
        }
        #endregion
    }
}