using UnityEngine;
using System.Collections;
using Blox;

namespace Blox.Messages
{
    public class LoadLevelMessage : CodeControl.Message
    {
        //** Public constructor for easy assembly
        public LoadLevelMessage(Types.LevelID eLevelID)
        {
            levelID = eLevelID;
        }

        //** Data
        public Types.LevelID levelID;

        #region Convenience static functions.
        public static void Register(string strMsgName, System.Action<LoadLevelMessage> callback)
        {
            CodeControl.Message.AddListener<Messages.LoadLevelMessage>(strMsgName, callback);
        }

        public static void Register(System.Action<LoadLevelMessage> callback)
        {
            Register("LoadLevel", callback);
        }
        
        public static void Unregister(string strMsgName, System.Action<LoadLevelMessage> callback)
        {
            CodeControl.Message.RemoveListener<Messages.LoadLevelMessage>("LoadLevel", callback);
        }

        public static void Unregister(System.Action<LoadLevelMessage> callback)
        {
            Unregister("LoadLevel", callback);
        }

        public static void Send(string strMsgName, LoadLevelMessage eMsg)
        {
            CodeControl.Message.Send<Messages.LoadLevelMessage>(strMsgName, eMsg);
        }

        public static void Send(LoadLevelMessage eMsg)
        {
            Send("LoadLevel", eMsg);
        }
        #endregion
    }
}