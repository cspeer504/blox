using UnityEngine;
using System.Collections;
using Blox;

namespace Blox.Messages
{
    public class PlayerScoreMessage : CodeControl.Message
    {
        //** Public constructor for easy assembly
        public PlayerScoreMessage(Types.PlayerID ePlayerID, int iScore)
        {
            PlayerID = ePlayerID;
            Score = iScore;
        }

        //** Data
        Types.PlayerID ePlayerID;
        public Types.PlayerID PlayerID
        {
            get { return ePlayerID; }
            set { ePlayerID = value; }
        }

        int iScore;
        public int Score
        {
            get { return iScore; }
            set { iScore = value; }
        }

        #region Convenience static functions.
        public static void Register(string strMsgName, System.Action<PlayerScoreMessage> callback)
        {
            CodeControl.Message.AddListener<Messages.PlayerScoreMessage>(strMsgName, callback);
        }

        public static void Register(System.Action<PlayerScoreMessage> callback)
        {
            Register("PlayerFinalScore", callback);
        }
        
        public static void Unregister(string strMsgName, System.Action<PlayerScoreMessage> callback)
        {
            CodeControl.Message.RemoveListener<Messages.PlayerScoreMessage>("PlayerFinalScore", callback);
        }

        public static void Unregister(System.Action<PlayerScoreMessage> callback)
        {
            Unregister("PlayerFinalScore", callback);
        }

        public static void Send(string strMsgName, PlayerScoreMessage eMsg)
        {
            CodeControl.Message.Send<Messages.PlayerScoreMessage>(strMsgName, eMsg);
        }

        public static void Send(PlayerScoreMessage eMsg)
        {
            Send("PlayerFinalScore", eMsg);
        }
        #endregion
    }
}