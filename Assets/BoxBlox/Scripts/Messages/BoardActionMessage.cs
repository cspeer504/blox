﻿using UnityEngine;
using System.Collections;
using Blox;

namespace Blox.Messages
{
    public class BoardActionMessage : CodeControl.Message
    {
        //** Public constructor for easy assembly
        public BoardActionMessage(Types.BoardAction eAction)
        {
            action = eAction;
        }

        //** Data
        public Types.BoardAction action = Types.BoardAction.None;

        #region Convenience static functions.
        public static void Register(string strMsgName, System.Action<BoardActionMessage> callback)
        {
            CodeControl.Message.AddListener<Messages.BoardActionMessage>(strMsgName, callback);
        }

        public static void Register(System.Action<BoardActionMessage> callback)
        {
            Register("BoardAction", callback);
        }
        
        public static void Unregister(string strMsgName, System.Action<BoardActionMessage> callback)
        {
            CodeControl.Message.RemoveListener<Messages.BoardActionMessage>("BoardAction", callback);
        }

        public static void Unregister(System.Action<BoardActionMessage> callback)
        {
            Unregister("BoardAction", callback);
        }

        public static void Send(string strMsgName, BoardActionMessage eMsg)
        {
            CodeControl.Message.Send<Messages.BoardActionMessage>(strMsgName, eMsg);
        }

        public static void Send(BoardActionMessage eMsg)
        {
            Send("BoardAction", eMsg);
        }
        #endregion
    }
}