﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Gamelogic.Grids;
using Gamelogic.Grids.Examples;


public class MatchGrid : GridBehaviour<RectPoint>
{
    public float animationTimePerCell = .1f;
    public int matchCount = 2;
    public bool m_bEnabled = true;
    public AudioSource matchSound;

    public Color[] cellColors;
    
    public bool useAcceleration = true;

	private RectGrid<MatchCell> matchGrid;
    private Blox.Types.MatchRule m_eMatchRule = Blox.Types.MatchRule.SameNodeType;
    private Blox.Types.MatchType m_eMatchType = Blox.Types.MatchType.Normal;
    private ulong m_ulCellID = 0;

    public bool IsColorBoost(Blox.Types.TileColor tileColor)
    {
        bool bReturn = false;

        switch (m_eMatchType)
        {
            case Blox.Types.MatchType.Normal:
                break;
            case Blox.Types.MatchType.ColorBoostRed25:
                bReturn = tileColor == Blox.Types.TileColor.Red;
                break;
            case Blox.Types.MatchType.ColorBoostBlue25:
                bReturn = tileColor == Blox.Types.TileColor.Blue;
                break;
            case Blox.Types.MatchType.ColorBoostGreen25:
                bReturn = tileColor == Blox.Types.TileColor.Green;
                break;
            case Blox.Types.MatchType.ColorBoostYellow25:
                bReturn = tileColor == Blox.Types.TileColor.Yellow;
                break;
            case Blox.Types.MatchType.QuantityBoost1:
                break;
            default:
                break;
        }
        return bReturn;
    }

    public void SetMatchRule(Blox.Types.MatchRule eRule)
    {
        m_eMatchRule = eRule;

        switch(eRule)
        {
            case Blox.Types.MatchRule.SameNodeType:
                matchGrid.SetNeighborsMain();
                break;
            case Blox.Types.MatchRule.SurroundingNodes:
                matchGrid.SetNeighborsMainAndDiagonals();
                break;
            default:
                break;
        }
    }

    public void SetMatchType(Blox.Types.MatchType eType)
    {
        m_eMatchType = eType;

        switch (eType)
        {
            case Blox.Types.MatchType.Normal:
                matchGrid.Apply(DisableColorLerp);
                Debug.Log("DisableColorLerp"); //** TODO: Remove me
                break;

            case Blox.Types.MatchType.QuantityBoost1:
                matchGrid.Apply(DisableColorLerp);
                Debug.Log("DisableColorLerp"); //** TODO: Remove me
                break;

            case Blox.Types.MatchType.ColorBoostRed25:
                matchGrid.Apply(EnableColorLerp);
                Debug.Log("ColorBoostRed25"); //** TODO: Remove me
                break;

            case Blox.Types.MatchType.ColorBoostBlue25:
                matchGrid.Apply(EnableColorLerp);
                Debug.Log("ColorBoostBlue25"); //** TODO: Remove me
                break;

            case Blox.Types.MatchType.ColorBoostGreen25:
                matchGrid.Apply(EnableColorLerp);
                Debug.Log("ColorBoostGreen25"); //** TODO: Remove me
                break;

            case Blox.Types.MatchType.ColorBoostYellow25:
                matchGrid.Apply(EnableColorLerp);
                Debug.Log("ColorBoostYellow25"); //** TODO: Remove me
                break;

            default:
                break;
        }
    }


    public void SetActive(bool bFlag)
    {
        m_bEnabled = bFlag;
    }

    public int GetWidth()
    {
        return matchGrid.Width;
    }

    public int GetHeight()
    {
        return matchGrid.Height;
    }

    public override void InitGrid()
	{
		matchGrid = (RectGrid<MatchCell>) Grid.CastValues<MatchCell, RectPoint>();
		matchGrid.Apply(InitCell);
    }
    
	private void InitCell(MatchCell cell)
	{
		int colorIndex = Random.Range(0, cellColors.Length);
        
		cell.TileColor = colorIndex; 
		cell.Color = cellColors[colorIndex];
		cell.IsMoving = false;
        cell.Initialize(m_ulCellID++);
        EnableColorLerp(cell);
    }

    public void StopAll()
    {
        matchGrid.Apply(DisableColorLerp);
    }

    public void EnableColorLerp(MatchCell cell)
    {
        if (IsColorBoost((Blox.Types.TileColor) cell.TileColor))
            cell.StartCrazyColor();
    }

    public void DisableColorLerp(MatchCell cell)
    {
        if(cell != null)
        {
            cell.StopColorChange();
        }
    }

    public void RandomizeGridInstantly()
    {
        //** Instant
        matchGrid.Apply(InitCell);
    }
    

    private IEnumerator RandomizeGrid()
    {
        bool bWasActive = m_bEnabled;
        foreach (RectPoint point in matchGrid)
        {
            InitCell(matchGrid[point]);
            yield return new WaitForSeconds(0.1f);
        }

        SetActive(bWasActive);
    }

    public void RemoveCell(RectPoint rectPoint)
    {
        var matchCell = matchGrid[rectPoint];

        if (matchCell != null)
        {
            matchCell.StopColorChange();
            Destroy(matchCell.gameObject);
        }

        IGrid<int, RectPoint> emptyCellsBelowCount = CountEmptyCellsBelowEachCell();
        StartMovingCells(emptyCellsBelowCount);

        int[] emptyCellsBelowTopCount = CountEmptyCellsBelowTop();
        MakeNewCellsAndStartMovingThem(emptyCellsBelowTopCount);
    }
    
    public void OnClick(RectPoint clickedPoint)
	{
		if (matchGrid.Values.Any(c => c == null || c.IsMoving || !m_bEnabled)) //If any cell is moving, ignore input
		{
			return;
		}

        var connectedSet = GetMatches(clickedPoint);
        
        if(connectedSet.Count > 0 && matchSound != null)
        {
            matchSound.Play();
        }
        //** TODO: Need m_eMatchType == ColorBoost25 to only boost one color type.
        Blox.Messages.MatchMessage.Send(new Blox.Messages.MatchMessage(connectedSet.Count, m_eMatchType, (Blox.Types.TileColor)matchGrid[clickedPoint].TileColor, Vector2.zero)); //** TODO: Change.
        DestroyMatchedCells(connectedSet);

		IGrid<int, RectPoint> emptyCellsBelowCount = CountEmptyCellsBelowEachCell();
		StartMovingCells(emptyCellsBelowCount);

		int[] emptyCellsBelowTopCount = CountEmptyCellsBelowTop();
		MakeNewCellsAndStartMovingThem(emptyCellsBelowTopCount);
	}

    private HashSet<RectPoint> GetMatches(RectPoint rectClickPoint)
    {
        var hashMatches = new HashSet<RectPoint>();
        
        switch (m_eMatchRule)
        {
            case Blox.Types.MatchRule.SameNodeType:
                hashMatches = Algorithms.GetConnectedSet(matchGrid, rectClickPoint, CheckTypeMatch);

                if (hashMatches.Count < matchCount)
                {
                    hashMatches.Clear();
                }
                /*
                foreach (RectPoint rect in hashMatches)
                {
                    Debug.Log("Match at " + rect.ToString());
                }*/
                    break;

            case Blox.Types.MatchRule.SurroundingNodes:
                RectPoint[] aNeighbors = matchGrid.NeighborDirections.ToArray();
                for (uint uiIndex = 0; uiIndex < aNeighbors.Length; ++uiIndex)
                {
                    RectPoint rect = aNeighbors[uiIndex] + rectClickPoint;
                    if (rect.X >= 0 && rect.X < matchGrid.Width && rect.Y >= 0 && rect.Y < matchGrid.Width)
                    {
                        hashMatches.Add(rect);                    
                    }
                }
                hashMatches.Add(rectClickPoint); //Also add current pick
                break;

            default:
                break;

        }
        return hashMatches;
    }

    private bool CheckTypeMatch(RectPoint p, RectPoint q)
	{
		if (matchGrid[p] == null) return false;
		if (matchGrid[q] == null) return false;

        bool bMatch = matchGrid[p].TileColor == matchGrid[q].TileColor;        
        return bMatch;
    }

	private void DestroyMatchedCells(IEnumerable<RectPoint> connectedSet)
	{
		foreach (var rectPoint in connectedSet)
		{
			var matchCell = matchGrid[rectPoint];

			if (matchCell != null)
			{
                matchCell.BreakCell();
                matchCell.StopColorChange();
                Destroy(matchCell.gameObject);
			}

			matchGrid[rectPoint] = null;
		}
	}

	//Start moving cells that have empty cells below them
	private void StartMovingCells(IGrid<int, RectPoint> emptyCellsBelowCount)
	{
		foreach (var point in emptyCellsBelowCount.WhereCell(c => c > 0).Where(point => matchGrid[point] != null))
		{
			StartCoroutine(MoveCell(point, matchGrid[point], emptyCellsBelowCount[point]));
		}
	}

	private void MakeNewCellsAndStartMovingThem(int[] emptyCellsBelowTopCount)
	{
		for (int columnIndex = 0; columnIndex < matchGrid.Width; columnIndex++)
		{
			for (int i = 0; i < emptyCellsBelowTopCount[columnIndex]; i++)
			{
				var point = new RectPoint(columnIndex, matchGrid.Height + i);
				var newCell = MakeNewCell(point);
                newCell.transform.rotation = gameObject.transform.rotation;

                StartCoroutine(MoveCell(point, newCell, emptyCellsBelowTopCount[columnIndex]));
			}
		}
	}

	private int[] CountEmptyCellsBelowTop()
	{
		var topRowEmptyCellsBelowCount = new int[matchGrid.Width];

		for (int columnIndex = 0; columnIndex < matchGrid.Width; columnIndex++)
		{
			var point = new RectPoint(columnIndex, matchGrid.Height);
			var pointBelow = point + RectPoint.South;
			int count = 0;

			while (matchGrid.Contains(pointBelow))
			{
				if (matchGrid[pointBelow] == null)
				{
					count++;
				}

				pointBelow += RectPoint.South;
			}

			topRowEmptyCellsBelowCount[columnIndex] = count;
		}
		return topRowEmptyCellsBelowCount;
	}

	private IGrid<int, RectPoint> CountEmptyCellsBelowEachCell()
	{
		var emptyCellsBelowCount = matchGrid.CloneStructure<int>();

		foreach (var point in matchGrid)
		{
			var pointBelow = point + RectPoint.South;
			int count = 0;

			while (matchGrid.Contains(pointBelow))
			{
				if (matchGrid[pointBelow] == null)
				{
					count++;
				}

				pointBelow += RectPoint.South;
			}

			emptyCellsBelowCount[point] = count;
		}
		return emptyCellsBelowCount;
	}

	private MatchCell MakeNewCell(RectPoint point)
	{
		var newCell = Instantiate(GridBuilder.CellPrefab).GetComponent<MatchCell>();

		newCell.transform.parent = transform;
		newCell.transform.localScale = Vector3.one;
		newCell.transform.localPosition = Map[point];

		InitCell(newCell);

		newCell.name = "-"; //set the name to empty until the cell has been put in the grid

		return newCell;
	}

	private IEnumerator MoveCell(RectPoint start, MatchCell cell, int numberOfCellsToMove)
	{
		return useAcceleration
			? MoveCellWithAcceleration(start, cell, numberOfCellsToMove)
			: MoveCellWithoutAcceleration(start, cell, numberOfCellsToMove);
	}

	private IEnumerator MoveCellWithoutAcceleration(RectPoint start, MatchCell cell, int numberOfCellsToMove)
	{
		cell.IsMoving = true;

		float totalTime = animationTimePerCell*numberOfCellsToMove;
		float time = 0;
		float t = 0;

		RectPoint destination = start + RectPoint.South*numberOfCellsToMove;

		while (t < 1)
		{
			time += Time.deltaTime;
			t = time/totalTime;

			var newPosition = Vector3.Lerp(Map[start], Map[destination], t);
			cell.transform.localPosition = newPosition;

			yield return null;
		}

		cell.transform.localPosition = Map[destination];
		matchGrid[destination] = cell;
		cell.name = destination.ToString(); //This allows us to see what is going on if we render gizmos

		cell.IsMoving = false;
	}

	private IEnumerator MoveCellWithAcceleration(RectPoint start, MatchCell cell, int numberOfCellsToMove)
	{
		cell.IsMoving = true;


		float displacement = 0;
		float t = 0;
		float speed = 0;
		const float acceleration = 10000;

		RectPoint destination = start + RectPoint.South*numberOfCellsToMove;
		float totalDisplacement = (Map[destination] - Map[start]).magnitude;

		while (t < 1)
		{
			speed += Time.deltaTime*acceleration;
			displacement += Time.deltaTime*speed;
			t = displacement/totalDisplacement;

			var newPosition = Map[start] + Vector3.down*displacement;
			cell.transform.localPosition = newPosition;

			yield return null;
		}

		cell.transform.localPosition = Map[destination];
		matchGrid[destination] = cell;
		cell.name = destination.ToString(); //This allows us to see what
		//is going on if we render gizmos

		cell.IsMoving = false;
	}
}