﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;
using Gamelogic.Grids;
using Gamelogic.Grids.Examples;

public class MatchCell : SpriteCell
{
	public int TileColor { get; set; }

	//We use this to block input while the cell is moving
	public bool IsMoving { get; set; }
    public float fColorLerpSpeed = 0.2f;
    private Color objOriginalColor;
    private string m_strTweenName = "";
    private ulong m_uiID = 0;
    private bool bIsFlashing = false;
    private float fStartTime = 0;
    private float fProgress = 0f;
    private Color objHighlightFromColor;
    private Color objHighlightToColor;


    public void Initialize(ulong ulID)
    {
        m_uiID = ulID;
        objHighlightToColor = new Color(Color.r *0.5f, Color.g * 0.5f, Color.b * 0.5f);
    }

    public void BreakCell()
    {
        GameObject objGo = Instantiate(Resources.Load("Prefabs/Cell/CellBreakParticle", typeof(GameObject))) as GameObject;
        objGo.transform.position = gameObject.transform.position;
        objGo.GetComponent<ParticleSystem>().startColor = Color;

        if(gameObject.transform.parent != null)
        {
            float fRotZ = gameObject.transform.parent.rotation.z;
            if (fRotZ < 1.01 && fRotZ > 0.09)
            {
                objGo.GetComponent<ParticleSystem>().gravityModifier *= -1;
            }
        }
    }

    public void StopColorChange()
    {
        OnColorUpdated(Color);
        bIsFlashing = false;
    }

    public void StartCrazyColor()
    {
        objHighlightFromColor = Color;
        bIsFlashing = true;
    }
    /*
    public void ToColor(Color fromColor, Color toColor)
    {
        Hashtable tweenParams = new Hashtable();
        tweenParams.Add("name", m_strTweenName);
        tweenParams.Add("from", fromColor);
        tweenParams.Add("to", toColor);
        tweenParams.Add("time", fColorLerpSpeed);
        tweenParams.Add("looptype", iTween.LoopType.pingPong);
        tweenParams.Add("onupdate", "OnColorUpdated");

        iTween.ValueTo(gameObject, tweenParams);
    }
    */

    public void OnColorUpdated(Color toColor)
    {
        SpriteRenderer.color = toColor;
    }

    void Update()
    {
        if(bIsFlashing)
        {
            fProgress = (Time.time / fColorLerpSpeed) % 1;

            if(fProgress < 0.5f)
            {
                SpriteRenderer.color = Color.Lerp(objHighlightFromColor, objHighlightToColor, fProgress);
            }
            else
            {
                SpriteRenderer.color = Color.Lerp(objHighlightToColor, objHighlightFromColor, fProgress);
            }
        }
    }

    void OnDestroy()
    {
        bIsFlashing = false;
    }
}